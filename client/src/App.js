import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { useEffect } from 'react';
import { useState } from 'react';
import { getUserFromToken } from './utils/Token';
import AuthContext from './providers/AuthContext';
import './App.css';
import HomePage from './components/Home/Home';
import Navigation from './components/Navigation/Navigation';
import MyOrders from './components/Users/Customers/Orders';
import Login from './components/RegisterAndLogin/LoginPage';
import Registration from './components/RegisterAndLogin/RegistrationForm';
import UserInfo from './components/Users/Customers/UserInfo';
import Warehouses from './components/Users/Employees/Warehouses/Warehouses';
import Shipments from './components/Users/Employees/Shipments/Shipments';
import CustomerGuardedRoute from './utils/CustomerGuardedRoute';
import EmployeeGuardedRoute from './utils/EmployeeGuardedRoute';
import Address from './components/Address/Address';
import AllParcels from './components/Users/Employees/Parcels/AllParcels';
import EmployeeHomePage from './components/Home/EmployeeHome';
import CustomerHomePage from './components/Home/CustomerHome';
import CreateParcel from './components/Users/Employees/Parcels/CreateParcel';
import About from './components/About/About';
import Parcel from './components/Users/Employees/Parcels/Parcel';
import usersPage from './components/UsersInfo/UsersInformation';
import CreateWarehouse from './components/Users/Employees/Warehouses/CreateWarehouse';
import CreateShipment from './components/Users/Employees/Shipments/CreateShipment';
import UserInfoEmployee from './components/Users/Employees/UserInfoEmployee/UserInfoEmployee';

function App() {
  const [user, setUser] = useState(getUserFromToken(localStorage.getItem('token')));

  useEffect(() => {
    if (!user) return;

    const timer = setTimeout(() => {
      setUser(null);
      localStorage.removeItem('token');
    }, 360000000);

    return () => clearTimeout(timer);
  }, [user]);

  return (
  <BrowserRouter>
    <AuthContext.Provider value={{ user, setUser }}>
          <Navigation />
              <Switch>
                <Redirect path="/" exact to="/home" />
                <Route exact path="/login" component={Login} />s
                <Route exact path="/register" component={Registration} />
                <Route exact path="/home" component={HomePage} />
                <Route exact path="/homeEmployee" component={EmployeeHomePage} />
                <Route exact path="/homeCustomer" component={CustomerHomePage} />
                <Route exact path="/about" component={About} />
                <Route exact path="/users" component={UserInfo} />
                <Route exact path="/users/employee" component={UserInfoEmployee} />
                <Route exact path="/users/info" component={usersPage} />
                <Route exact path="/address" component={Address} />
                <Route exact path="/shipments" component={Shipments} />
                <Route exact path="/shipments/create" component={CreateShipment} />
                <Route exact path="/parcels" component={AllParcels} />
                <Route exact path="/parcels/create" component={CreateParcel} />
                <Route exact path="/warehouses/create" component={CreateWarehouse} />
                <Route exact path="/parcels/parcel/:id" component={Parcel} />
                <CustomerGuardedRoute path="/orders" component={MyOrders} auth={user?.role} />
                <EmployeeGuardedRoute path="/warehouses" component={Warehouses} auth={user?.role} />
              </Switch>
    </AuthContext.Provider>
  </BrowserRouter >
  );
}

export default App;
