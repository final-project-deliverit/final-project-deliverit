import { Route, Redirect } from "react-router-dom";

const GuardedRoute = ({ component: Component, auth, ...rest }) => {
console.log("auth:" + auth)

        return(
            <Route {...rest} render={(props) => (
                auth === "customer" ?
                <Component {...props} />
                : <Redirect to='/homeCustomer' />  
            )} />
        );
};

export default GuardedRoute;