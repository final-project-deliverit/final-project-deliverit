import { Route, Redirect } from "react-router-dom";

const GuardedRoute = ({ component: Component, auth, ...rest }) => {
console.log("auth:" + auth)

        return(
            <Route {...rest} render={(props) => (
                auth === "employee" ?
                <Component {...props} />
                : <Redirect to='/homeEmployee' />  
            )} />
        );
};

export default GuardedRoute;