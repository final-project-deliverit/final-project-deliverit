import decode from 'jwt-decode';

export const getUserFromToken = (token) => {
    try {
        const user = decode(token);
        console.log(user);
        return user;
    } catch {
        localStorage.removeItem('token');
    }

    return null;
};