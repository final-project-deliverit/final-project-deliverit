import { createContext } from 'react';

const whContext = createContext({
    warehouses: null,
    setWarehouses: () => {}
})

export default whContext;