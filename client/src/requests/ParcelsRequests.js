import MAIN_URL from "../common/MainUrl";
export const getParcelsByCustomer = (id) => {
    return fetch(`${MAIN_URL}parcels/customer/${id}`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
  };
export const getParcelsByShipment = (id) => {
    return fetch(`${MAIN_URL}parcels/shipment/${id}`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
  };
export const getParcelsByStatus = (status_id) => {
    return fetch(`${MAIN_URL}parcels/status/${status_id}`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
  };

export const getAllParcels = () => {
  return fetch(`${MAIN_URL}parcels`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then((response) => response.json())
};

export const getParcelById = (id) => {
  return fetch(`${MAIN_URL}parcels/parcel/${id}`, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
  })
    .then((response) => response.json())
};

export const createParcel = (data) => {
  const request = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${localStorage.getItem('token')}`

    },
    body: JSON.stringify(data)
  };
return fetch(`${MAIN_URL}parcels`, request)
    .then((response) => response.json())
};
export const updateParcel = (data) => {
  const request = {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(data)
  };

  return fetch(`${MAIN_URL}parcels`, request)
      .then((response) => response.json());
};
export const updateShipmentId = (data) => {
  const request = {
      method: 'PUT',
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(data)
  };

  return fetch(`${MAIN_URL}parcels/shipment`, request)
      .then((response) => response.json());
};



