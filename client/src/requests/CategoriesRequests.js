import MAIN_URL from './../common/MainUrl';

export const getAllCategories = () => {
    return fetch(`${MAIN_URL}categories`, {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
          }
        })
          .then((response) => response.json())
};

