import MAIN_URL from "../common/MainUrl";

export const getAllShipments = () => {
    return fetch(`${MAIN_URL}shipments`, {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
        .then((response) => response.json())
};
export const getShipmentById = (id) => {
    return fetch(`${MAIN_URL}shipments/with/${id}`, {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
        .then((response) => response.json())
};
export const getShipmentsByStatus = (id) => {
    return fetch(`${MAIN_URL}shipments/status/${id}`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
  };
export const getShipmentsByWarehouse = (column) => {
    return fetch(`${MAIN_URL}shipments/by?column=${column}`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
  };
  export const updateShipment = (data) => {
    const request = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data)
    };

    return fetch(`${MAIN_URL}shipments`, request)
        .then((response) => response.json());
};
export const createShipment = (data) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
    };
    return fetch(`${MAIN_URL}shipments`, request)
        .then((response) => response.json())
};