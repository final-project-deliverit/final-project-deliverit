import MAIN_URL from "../common/MainUrl";

export const createUser = (data) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    };
    return fetch(`${MAIN_URL}users`, request)
};
export const loginUser = (data) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data)
    };

    return fetch(`${MAIN_URL}auth/signin`, request);

};
export const getCustomerById = (id) => {
    return fetch(`${MAIN_URL}users/${id}`, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
  };

  export const getAllCustomers = () => {
    return fetch(`${MAIN_URL}users`, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then((response) => response.json())
  };

  export const getUsers = (value) => {
    return fetch(`${MAIN_URL}users?q=${value}`, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
      })
        .then((response) => response.json())
  };


