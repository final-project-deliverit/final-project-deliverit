
import MAIN_URL from './../common/MainUrl';

export const getAllCountries = () => {
    return fetch(`${MAIN_URL}countries/`, {
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    })
        .then((response) => response.json())
};

export const getAllCities = () => {
    return fetch(`${MAIN_URL}cities/`, {
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    })
        .then((response) => response.json())
};

export const getCitiesByCountry = (id) => {
    return fetch(`${MAIN_URL}cities/${id}`, {
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    })
        .then((response) => response.json())
};

export const createAddress = (data) => {
    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    };
    return fetch(`${MAIN_URL}address`, request)
        .then((response) => response.json())
};

export const getAddressById = (id) => {
    return fetch(`${MAIN_URL}address/${id}`, {
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    })
        .then((response) => response.json())
}
export const updateAddress = (data) => {
    const request = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data)
    };

    return fetch(`${MAIN_URL}address`, request)
        .then((response) => response.json());

};