import MAIN_URL from "../common/MainUrl";

export const getPublicAllWarehouses = () => {
    return fetch(`${MAIN_URL}warehouses/public`, {
        headers: {
            'Access-Control-Allow-Origin': '*',
        }
    })
        .then((response) => response.json())
};

export const getAllWarehouses = () => {
    return fetch(`${MAIN_URL}warehouses`, {
        headers: {
            'Access-Control-Allow-Origin': '*',
        }
    })
        .then((response) => response.json())
};

export const updateWhData = (data) => {
    const request = {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data)
    };

    return fetch(`${MAIN_URL}warehouses`, request)
        .then((response) => response.json());

};
export const createNewWh = (data) => {

    const request = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
        body: JSON.stringify(data)
    };

    return fetch(`${MAIN_URL}warehouses`, request)
        .then(res => res.json());
};
export const getWarehouseById = (id) => {
    return fetch(`${MAIN_URL}warehouses/${id}`, {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
        .then((response) => response.json())
};