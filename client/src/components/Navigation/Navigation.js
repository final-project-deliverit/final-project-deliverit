import { Link } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import { Nav } from "react-bootstrap";
import { Container } from "react-bootstrap";
import { Button } from "react-bootstrap";
import { useContext, useState } from "react";
import AuthContext from "../../providers/AuthContext";
import { useHistory } from 'react-router-dom';
import Modal from 'react-bootstrap/Modal';
import DeliverITLogo from '../../media/DeliverITLogo.png';
import './Navigation.css';

const Navigation = () => {

  const { user, setUser } = useContext(AuthContext);

  const history = useHistory();


  const logOut = () => {
    setUser(null);
    localStorage.removeItem('token');
    history.push('/home');
  };

  function Example() {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
      <>
        <Nav.Link className="ctaHvr" onClick={handleShow}>
           <Link className="cta">Log out</Link> 
        </Nav.Link>

        <Modal
          show={show}
          onHide={handleClose}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Are you sure you want to log out?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Press Cancel to go back at the page
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Cancel
            </Button>
            <Button variant="primary" onClick={logOut}>
              Confirm
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  };


  return (
    <div>
      {user ?
        <div>
          <Navbar className="Navbar">
            <Container>
              <Navbar.Brand> 
                <img src={DeliverITLogo} alt='deliverit' className="logo" />
              </Navbar.Brand>
              <Nav className="me-auto">
                { user.role === "customer" 
                ? (<> 
                  <Nav.Link >              
                    <Link to='/homeCustomer' className="nav-links">
                        Home
                    </Link>
                  </Nav.Link>
                  <Nav.Link >
                    <Link to='/orders' className="nav-links">
                        My Orders
                    </Link>
                  </Nav.Link> 
                  <Nav.Link >
                    <Link to='/users' className="nav-links">
                        My Account
                    </Link>
                  </Nav.Link>
                  </>)
                : 
                <>
                <Nav.Link >              
                    <Link to='/homeEmployee' className="nav-links">
                        Home
                    </Link>
                  </Nav.Link>
                  <Nav.Link >
                    <Link to='/users/employee' className="nav-links">
                        My Account
                    </Link>
                  </Nav.Link>
                </>
                }
                <Example />
              </Nav>
            </Container>
          </Navbar>
        </div>
        :
        <div>
               <Navbar>
            <Container>
              <Navbar.Brand>
                <img className="logo" src={DeliverITLogo} alt="deliverit" />
              </Navbar.Brand>
              <Nav>
                <Nav.Link>
                  <Link to="/home" className="nav-links">
                    Home
                  </Link>
                </Nav.Link>
                <Nav.Link>
                  <Link to="/about" className="nav-links">
                    About us
                  </Link>
                </Nav.Link>
                <Nav.Link>
                  <Link to="/login" className="nav-links">
                    Login
                  </Link>
                </Nav.Link>
                <Nav.Link className="ctaHvr">
                  <Link to="/register" className="cta">
                    Register
                  </Link>
                </Nav.Link>
              </Nav>
            </Container>
          </Navbar>
        </div>
      }
    </div >
  )
};

export default Navigation;