import React, { useState, useContext, useEffect } from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { createUser, loginUser } from '../../requests/UsersRequests';
import 'bootstrap/dist/css/bootstrap.min.css';
import { getUserFromToken } from '../../utils/Token';
import { useHistory } from 'react-router';
import AuthContext from '../../providers/AuthContext';
import Form from 'react-bootstrap/Form';
import { createAddress, getAllCountries, getCitiesByCountry, getAddressById } from './../../requests/AddressRequest';
import heroForm from "../../media/hero-forms.jpg";
import "./Forms.css";

const Registration = () => {

    const history = useHistory();

    const { setUser } = useContext(AuthContext);

    const [registration, setRegistration] = useState({
        username: '',
        password: '',
        addresses_id: 0,
        first_name: '',
        last_name: '',
        email: '',
    })

    const [duplicateMessage, setDuplicateMessage] = useState(false);
    const [successMessage, setSuccessMessage] = useState(false);
    const [minUsernameLength, setMinUserNameLength] = useState(true);
    const [minPasswordLength, setMinPasswordNameLength] = useState(true); 
    const [minStreetLength, setMinStreetLength] = useState(false); 


    const login = (e, data) => {
        e.preventDefault();
        loginUser(data)
            .then((res) => res.json())
            .then(response => {
                setUser(getUserFromToken(response.token));
                localStorage.setItem('token', response.token);
            });
        history.push('/homeCustomer');
    }

    const create = (prop, value) => {
        registration[prop] = value;
        setRegistration({ ...registration })
    };

    const registerUser = (data) => {
        createUser(data)
            .then((response) => {
                if (response.status === 409) {
                    setDuplicateMessage(true);
                    return null;
                }
                setDuplicateMessage(false);
                setSuccessMessage(true);
                response.json();
            });
    }

    // Address
    const [countries, setCountries] = useState([]);
    const [countryId, setCountryId] = useState(0);
    const [cities, setCities] = useState([]);
    const [showAddress, setShowAddress] = useState(false);
    const [newAddressId, setNewAddressId] = useState(0);
    const [addressToPrint, setAddressToPrint] = useState('');
    const [address, setAddress] = useState({
        cities_id: 0,
        street_name: '',
    })

    const createNewAddress = (prop, value) => {
        address[prop] = value;
        setAddress({ ...address })
    };

    const registerAddressAndUser = (data) => {
        createAddress(data)
            .then((response) => {
                const addressId = response.id;
                create('addresses_id', addressId);
            })
            .then(() => registerUser(registration))
        };

    useEffect(() => {
        getAllCountries()
        .then(data => setCountries(data))
    }, []);

    useEffect(() => {
        getCitiesByCountry(countryId)
        .then(data => setCities(data))
    }, [countryId]);

    useEffect(() => {
        getAddressById(newAddressId)
        .then(data => data[0])
        .then(data => {
            if (data) {
                return setAddressToPrint(`${data.country}, ${data.city}, ${data.street}`);
            }
        })
    }, [newAddressId])

    //

    return (
        <div>
      <div className="container-forms">
        <div className="left-reg">
          <h1>Register new account</h1>
          <Form>
            <Form.Group as={Col} controlId="formBasicAddress">
              <div>
                <b>Account information</b>
              </div>
              <hr />
              <Row>
                <Form.Group as={Col} controlId="formBasicUsername">
                  <Form.Control
                    maxLength={15}
                    onClick={() => setDuplicateMessage(false)}
                    onChange={(e) => {
                      if (e.target.value.length < 5) {
                        setMinUserNameLength(true);
                      } else {
                        setMinUserNameLength(false);
                      }
                      create("username", e.target.value);
                    }}
                    type="username"
                    placeholder="Username"
                  />
                  <div style={{ fontSize: 13, marginTop: 5 }}>
                    Between 5 - 15 symbols
                  </div>
                </Form.Group>
                {duplicateMessage ? (
                  <div style={{ color: "red" }}>Username already exist!</div>
                ) : null}
                <Form.Group as={Col} controlId="formBasicPassword">
                  <Form.Control
                    maxLength={20}
                    onClick={() => setDuplicateMessage(false)}
                    onChange={(e) => {
                      if (e.target.value.length < 5) {
                        setMinPasswordNameLength(true);
                      } else {
                        setMinPasswordNameLength(false);
                      }
                      create("password", e.target.value);
                    }}
                    type="password"
                    placeholder="Password"
                  />
                  <div style={{ fontSize: 13, marginTop: 5 }}>
                    At least 5 symbols
                  </div>
                </Form.Group>
              </Row>
            </Form.Group>
            <Form.Group as={Col} controlId="formBasicAddress">
              <div style={{ marginTop: '15px'}}>
                <b>Personal information</b>
              </div>
              <hr />
              <Row>
                <Form.Group as={Col} style={{ marginTop: '6px'}} controlId="formBasicFirstName">
                  <Form.Control
                    maxLength={20}
                    onChange={(e) => create("first_name", e.target.value)}
                    type="name"
                    placeholder="First Name"
                  />
                </Form.Group>
                <Form.Group as={Col} style={{ marginTop: '6px'}} controlId="formBasicLastName">
                  <Form.Control
                    maxLength={20}
                    onChange={(e) => create("last_name", e.target.value)}
                    type="name"
                    placeholder="Last Name"
                  />
                </Form.Group>
              </Row>
              <Row>
                <Form.Group as={Col} style={{ marginTop: '13px'}} controlId="formBasicEmail">
                  <Form.Control
                    maxLength={20}
                    onChange={(e) => create("email", e.target.value)}
                    type="email"
                    placeholder="Email"
                  />
                </Form.Group>
              </Row>
            </Form.Group>
            <Form.Group as={Col} controlId="formBasicAddress">
              <div style={{ marginTop: '12px'}}>
                <b>Address</b>
              </div>
              <hr />
              {showAddress ? (
                <div>{addressToPrint}</div>
              ) : (
                <Row>
                  <Form.Group as={Col} controlId="formGridState">
                    <Form.Control
                      as="select"
                      onChange={(e) => setCountryId(e.target.value)}
                      type="select"
                    >
                      <option value="0">Country...</option>
                      {countries.map((country) => (
                        <option value={country.id} key={country.id}>
                          {country.country}
                        </option>
                      ))}
                    </Form.Control>
                  </Form.Group>
                  <Form.Group as={Col} controlId="formBasicLastName">
                    <Form.Control
                      as="select"
                      onChange={(e) =>
                        createNewAddress("cities_id", e.target.value)
                      }
                      type="select"
                    >
                      <option value="0">City...</option>
                      {cities.length > 0 &&
                        cities.map((city) => (
                          <option value={city.id} key={city.id}>
                            {city.city}
                          </option>
                        ))}
                    </Form.Control>
                  </Form.Group>
                  <Form.Group as={Col} controlId="formBasicLastName">
                    <Form.Control
                      maxLength={20}
                      onChange={(e) => {
                        if (e.target.value.length > 5) {
                          setMinStreetLength(true);
                        } else {
                          setMinStreetLength(false);
                        }
                        createNewAddress("street_name", e.target.value);
                      }}
                      type="street"
                      placeholder="Street"
                    />
                  </Form.Group>
                </Row>
              )}
            </Form.Group>
            {successMessage ? (
              <div style={{ textAlign: "center" }}>
                <div style={{ color: "green" }}>
                  You have registered successfully!
                </div>
                <Button
                  variant="primary"
                  onClick={(e) => {
                    login(e, registration);
                  }}
                  type="submit"
                >
                  Login
                </Button>
              </div>
            ) : (
              <div>
                {
                  <Button
                    variant="primary"
                    onClick={() => {
                      registerAddressAndUser(address);
                    }}
                    style={{ marginTop: '20px', marginLeft: '20px'}}
                  >
                    Confirm
                  </Button>
                }
              </div>
            )}
          </Form>
        </div>
        <div className="right-reg">
          <img src={heroForm} alt="Hero banner" />
        </div>
      </div>
    </div>
    )
};

export default Registration;