import React, { useContext, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useHistory } from 'react-router-dom';
import { loginUser } from '../../requests/UsersRequests';
import AuthContext from '../../providers/AuthContext';
import { getUserFromToken } from '../../utils/Token';
import heroForm from "../../media/hero-forms.jpg";
import "./Forms.css";

const Login = () => {

    const { user, setUser } = useContext(AuthContext);

    const [errorMessage, setErrorMessage] = useState(false);

    const [loginInfo, setLoginInfo] = useState({
        username: '',
        password: '',
    })

    const collectLoginData = (prop, value) => {
        loginInfo[prop] = value;
        setLoginInfo({ ...loginInfo })
    };

    const history = useHistory();

    const login = (e, data) => {
        e.preventDefault();
        loginUser(data)
            .then((response) => {
                if (response.status === 400) {
                    setErrorMessage(true)
                    return null;
                }
                response.json()
                    .then((res) => {
                        const newUser = getUserFromToken(res.token);
                        setUser(getUserFromToken(res.token));
                        localStorage.setItem('token', res.token);
                        return newUser;
                    })
                    .then((newUser) => {
                        console.log('logged user', newUser);
                        if (newUser?.role === 'employee') {
                            history.push('/homeEmployee');
                        } else if (newUser?.role === 'customer') {
                            history.push('/homeCustomer');
                        } else {
                            history.push('/home');
                        }
                    })
            })
        ;
    }

    return (
        <div>
        <div className="container-forms">
          <div className="left-login">
            <h1>Login to your account</h1>
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Username</Form.Label>
                <Form.Control
                  maxLength={20}
                  onClick={() => setErrorMessage(false)}
                  type="username"
                  placeholder="Enter username"
                  onChange={(e) => {
                    collectLoginData("username", e.target.value);
                  }}
                />
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  maxLength={20}
                  onClick={() => setErrorMessage(false)}
                  onChange={(e) => {
                    collectLoginData("password", e.target.value);
                  }}
                  type="password"
                  placeholder="Password"
                />
              </Form.Group>
              <Form.Group controlId="formBasicCheckbox"></Form.Group>
              {errorMessage ? (
                <div
                  style={{
                    color: "black",
                    marginBottom: "10px",
                    fontSize: "20px",
                  }}
                >
                  Invalid username or password!
                </div>
              ) : null}
              <Button
                className="btn-login"
                onClick={(e) => {
                  login(e, loginInfo);
                }}
                type="submit"
              >
                Login
              </Button>
            </Form>
          </div>
          <div className="right-login">
            <img src={heroForm} alt="Hero banner" />
          </div>
        </div>
      </div>

    )
}


export default Login;