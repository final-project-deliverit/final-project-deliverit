import { useEffect, useState } from "react";
import { getAllWarehouses, getPublicAllWarehouses } from './../../../requests/WarehousesRequests';
import PublicSingleWarehouse from './PublicSingleWarehouse';


const PublicWarehouses = ({refProp}) => {

    const [ warehouses, setWarehouses ] = useState([]);

    useEffect(() => {
        getAllWarehouses()
            .then(data => setWarehouses(data))
    }, [])

    return (
        <div id="warehouse">
            <h1>Warehouses</h1>
            <div ref={refProp} className="container-cards">{warehouses.map(wh => 
                <PublicSingleWarehouse key={wh.id} wh={wh}/>  
            )}</div>
        </div>
    );
};

export default PublicWarehouses;