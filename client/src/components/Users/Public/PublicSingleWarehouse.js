import { Card} from 'react-bootstrap';
import { FaWarehouse } from "react-icons/fa";

const PublicSingleWarehouse = ({ wh }) => {

    return (
        <div className="singleCard">
            <h3>Warehouse</h3>
            <span>
              <FaWarehouse />
            </span>
            <h1>{wh.warehouse}</h1>
            <h2>{wh.street_name + ", " + wh.city + ", " + wh.country}</h2>
          </div>
    );
};

export default PublicSingleWarehouse;