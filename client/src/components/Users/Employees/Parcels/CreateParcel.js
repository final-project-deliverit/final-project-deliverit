// import { format } from 'date-fns';
import { useEffect, useState } from 'react';
import { Button, Form, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { createParcel } from './../../../../requests/ParcelsRequests';
import { getAllWarehouses } from './../../../../requests/WarehousesRequests';
import { getAllShipments } from './../../../../requests/ShipmentsRequests';
import { getAllCategories } from './../../../../requests/CategoriesRequests';
import { getAllCustomers } from '../../../../requests/UsersRequests';
import VerticalNav from '../../../VerticalNav/VerticalNav';

const CreateParcel = () => { 
    const [ customers, setCustomers ] = useState([]);
    const [ categories, setCategories ] = useState([]);
    const [ warehouses, setWarehouses ] = useState([]);
    const [ warehouseId, setWarehouseId ] = useState(0);
    const [ shipments, setShipments ] = useState([]);
    const [newParcel, setNewParcel] = useState({
        weight: 0,
        warehouses_id: 0,
        categories_id: 0,
        shipments_id: 0,
        users_id: 0,
        delivered_to_address: 0,
    })

    const create = (prop, value) => {
        newParcel[prop] = value;
        setNewParcel({ ...newParcel })
    };


    const addParcel = () => {
        createParcel(newParcel)
            .then(res => console.log(res));
    };


    useEffect(() => {
        getAllCustomers()
            .then(data => setCustomers(data))
    }, [])

    useEffect(() => {
        getAllWarehouses()
            .then(data => setWarehouses(data))
    }, []);
    useEffect(() => {
            getAllCategories()
                .then(data => setCategories(data))
    }, []);
    useEffect(() => {
        getAllShipments()
            .then(data => setShipments(data))
    }, []);
            

    const labelStyle = { fontSize: '15px', color: 'black', marginLeft: '10px', display: 'flex' };
    
    return ( 
        <div>
            <VerticalNav />
        <Form style={{ width: '55%', marginBlock: '30px', marginInline: 'auto' }}>
            <h3 style={{ fontFamily: 'cursive', color: '#4042e2', fontWeight: '600'}}>Create new parcel</h3>
            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ labelStyle }>Customer</Form.Label>
                <Form.Control as="select" 
                    onChange={(e) => create('users_id', e.target.value)}
                    type="select" >
                <option value="0">Customer...</option>
                {customers
                    .map(customer => 
                            <option value={customer.id} key={customer.id}>
                                {customer.first_name + " " + 
                                customer.last_name + ", " + 
                                customer.email}
                            </option>
                        )
                }  
                </Form.Control>    
            </Form.Group>
            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ labelStyle }>Weight</Form.Label>
                <Form.Control
                    onChange={(e) => create('weight', e.target.value)}
                    type="text" >
                </Form.Control>
            </Form.Group>
            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ labelStyle }>Category</Form.Label>
                <Form.Control as="select" 
                    onChange={(e) => create('categories_id', e.target.value)}
                    type="select" >
                    <option value="0">Category...</option>
                    {categories
                            .map(category => 
                                <option value={category.id} key={category.id}>
                                    {category.category}
                                </option>)}  
                </Form.Control>    
            </Form.Group>
            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ labelStyle }>Arrival warehouse</Form.Label>
                <Form.Control as="select" 
                    onChange={(e) => {
                        create('warehouses_id', e.target.value);
                        setWarehouseId(e.target.value);
                    }}
                    type="select" >
                    <option value="0">Warehouse...</option>
                    {warehouses
                        .map(wh => 
                                <option value={wh.id} key={wh.id} name={wh.warehouse}>
                                    {wh.warehouse + ", " + 
                                    wh.street_name + ", " + 
                                    wh.city + ", " + 
                                    wh.country}
                                </option>
                            )
                    }  
                </Form.Control>    
            </Form.Group>
            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ labelStyle }>Shipment</Form.Label>
                <Form.Control as="select" 
                    onChange={(e) => create('shipments_id', e.target.value)}
                    type="select" >
                <option value="0">Shipment...</option>
                {shipments
                    .filter(shipment => shipment.status === 'preparing')
                    .filter(shipment => shipment.delivery_wh_id === +warehouseId)
                    .map(shipment => 
                            <option value={shipment.id} key={shipment.id}>
                                {shipment.origin_wh + ", " + 
                                shipment.delivery_wh + ", " + 
                                new Date(shipment.departure_date).toLocaleDateString('fr-CA')  + ", " + 
                                new Date(shipment.arrival_date).toLocaleDateString('fr-CA')  + ", " + 
                                shipment.status}
                            </option>   
                        )
                }  

                </Form.Control>    
            </Form.Group>

            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ labelStyle }>Delivery</Form.Label>
                <Form.Control as="select" 
                    onChange={(e) => create('delivered_to_address', e.target.value)}
                    type="select" >
                <option value="0">pick up</option>
                <option value="1">to address</option>  
                </Form.Control>    
            </Form.Group>
            <Link to="/parcels" >
                <Button variant='primary' style={{margin: '1rem 1rem 1rem 3rem'}}>
                    Cancel
                </Button> 
            </Link>
            <Link to="/parcels" >
                <Button variant='primary' style={{margin: '1rem'}} onClick={() => {addParcel()}}>
                    Confirm
                </Button> 
            </Link>
        </Form>
        </div>
    );
};

export default CreateParcel;

