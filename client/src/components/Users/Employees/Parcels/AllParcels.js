import { useEffect, useState } from "react";
import { getAllParcels } from "../../../../requests/ParcelsRequests";
import { Button, Nav } from 'react-bootstrap';
import VerticalNav from './../../../VerticalNav/VerticalNav';
import SingleParcel from './SingleParcel';
import { Link } from 'react-router-dom';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import STATUS from './../../../../common/Statuses';


const AllParcels = () => {
    
    const [ parcels, setParcels ] = useState([]);
    const [ parcelByStatus, setParcelByStatus ] = useState([]);

    useEffect(() => {
        getAllParcels()
            .then(data => {
                setParcels(data)
                setParcelByStatus(data)
                }
            )
    }, [])
    
    console.log(parcels);

    const filterParcels = (status_id) => {
        const p = status_id === 0 ? parcels : parcels.filter(el => el.status_id === status_id);
        
        return (p.length !== 0) ? p : [{ msg: 'You have no parcels with this status' }];
    };


    return (
        <div>
            <VerticalNav />
                
            <div className="filter-container">
                <div className="Switch">
                    <div className="parcels-header">
                        <h2>Parcels</h2>

                        <ButtonGroup aria-label="Basic example">
                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(0))} >All Parcels</Button>
                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(STATUS.PREPARING)) } >Preparing</Button>
                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(STATUS.ON_THE_WAY))} >On the way</Button>

                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(STATUS.COMPLETED))} >Completed</Button>
                        </ButtonGroup>
                        <Nav.Link >
                        <Link to="/parcels/create">
                            <Button variant='success'>
                                Create Parcel
                            </Button>
                        </Link>
                    </Nav.Link>   
                    </div>
                    {/* <h3>Count {parcelByStatus.length}</h3> */}
                             
                    <div>
                        {parcelByStatus.map(parcel => 
                            <div key={parcel.parcel}>
                                { parcel.parcel > 0
                                    ? <SingleParcel key={parcel.parcel} parcel={parcel} /> 
                                    : <p>{parcel.msg}</p>
                                } 
                            </div>)
                        }
                    </div>
                </div>  
            </div>
        </div>
    );
};

export default AllParcels;

