// import { useState } from 'react';
import { Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';



const SingleParcel = ({ parcel }) => {

    return (
        <Card border="primary" style={{ width: '70rem', marginBottom: '10px', marginTop: '15px'}}>
            <Card.Header >
                 <Link to={`/parcels/parcel/${parcel.parcel}`} >
                    <span style={{ fontStyle: "oblique", fontFamily: "cursive", color: '#4042e2', fontSize: '20px' }}> &nbsp;
                        Parcel &nbsp; {parcel.parcel}
                    </span> 
                </Link>
            </Card.Header>
            <Card.Body>
                <Card.Title>
                    <Row>
                        <Col>costumer  </Col>
                        <Col>weight    </Col>
                        <Col>category  </Col>
                        <Col>warehouse delivery</Col>
                        <Col>shipment  </Col>
                        <Col>status  </Col>
                        <Col>delivery</Col>
                    </Row>  
                </Card.Title>
                <Card.Text>
                    <Row>
                        <Col>{parcel.costumer}</Col>
                        <Col>{parcel.weight} kg</Col>
                        <Col>{parcel.category}</Col>
                        <Col>{parcel.warehouse}</Col>
                        <Col>{parcel.shipment}</Col>
                        <Col>{parcel.status}</Col>
                        <Col>{ parcel.delivered_to_address === 1 ? 'to address' : 'pick up' }</Col>
                    </Row>           
                </Card.Text>
            </Card.Body>
        </Card>
    );
};

export default SingleParcel;