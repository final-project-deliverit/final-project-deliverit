import { useEffect, useState } from 'react';
import { Card, Button, Form, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getParcelById } from '../../../../requests/ParcelsRequests';
import { updateParcel } from './../../../../requests/ParcelsRequests';
import { getAllCustomers } from '../../../../requests/UsersRequests';
import { getAllWarehouses } from './../../../../requests/WarehousesRequests';
import { getAllCategories } from './../../../../requests/CategoriesRequests';
import { getAllShipments } from './../../../../requests/ShipmentsRequests';
import SingleParcel from './SingleParcel';
import VerticalNav from '../../../VerticalNav/VerticalNav';

const Parcel = ({ match }) => { 
    const { id } = match.params;
    const [ parcel, setParcel ] = useState(null);
    const [ editMode, setEditMode ] = useState(false);
    const [ updatedParcel, setUpdatedParcel ] = useState({
        id: id,
        weight: 0,
        warehouses_id: 0,
        categories_id: 0,
        shipments_id: 0,
        users_id: 0,
        delivered_to_address: 0,
    });
    const [ customers, setCustomers ] = useState([]);
    const [ categories, setCategories ] = useState([]);
    const [ warehouses, setWarehouses ] = useState([]);
    const [ warehouseId, setWarehouseId ] = useState(0);
    const [ shipments, setShipments ] = useState([]);
    
    useEffect(() => {
        getParcelById(id)
        .then(data => data[0])
        .then(data => {
            setParcel(data);
            create('weight', data.weight);
            create('warehouses_id', data.warehouses_id);
            create('categories_id', data.categories_id);
            create('shipments_id', data.shipments_id);
            create('users_id', data.users_id);
            create('delivered_to_address', data.delivered_to_address);
        });
        
    }, [id, editMode])
    
    console.log('parcel', parcel);
    console.log('updatedParcel', updatedParcel);

    useEffect(() => {
        getAllCustomers()
            .then(data => setCustomers(data))
    }, [])
    
    useEffect(() => {
        getAllWarehouses()
            .then(data => setWarehouses(data))
    }, []);
    useEffect(() => {
            getAllCategories()
                .then(data => setCategories(data))
    }, []);
    useEffect(() => {
        getAllShipments()
            .then(data => setShipments(data))
    }, []);


    const create = (prop, value) => {
        updatedParcel[prop] = value;
        setUpdatedParcel({ ...updatedParcel })
    };
    
    const sendParcel = (data) => {
        
        updateParcel(data)
        .then(res => console.log('updateP', res));
    };
    
    if (parcel === null) {
        return <div>Parcel is loading...</div>
    }

    return (
        <div>
            <VerticalNav />
        <Form style={{ width: '60%', marginBlock: '9rem', marginInline: '15rem' }}> 
            {
                editMode                
                ? <Card border="primary" style={{ width: '70rem', marginBottom: '10px', marginTop: '15px'}}>
                    <Card.Header >
                        <span style={{ fontStyle: "oblique", fontFamily: "cursive" }}> &nbsp;
                            Parcel &nbsp;
                        </span> 
                        {parcel.parcel}
                    </Card.Header>
                    <Card.Body>
                    <Card.Title>
                        <Row>
                            <Col>costumer  </Col>
                            <Col>weight    </Col>
                            <Col>category  </Col>
                            <Col>warehouse delivery</Col>
                            <Col>shipment  </Col>
                            <Col>status  </Col>
                            <Col>delivery</Col>
                        </Row>  
                    </Card.Title>
                    <Card.Text>
                        <Row>
                            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                                <Form.Control as="select" 
                                    onChange={(e) => create('users_id', e.target.value)}
                                    type="select" >
                                <option value="0"> {parcel.costumer} </option>
                                {customers
                                    .map(customer => 
                                            <option value={customer.id} key={customer.id}>
                                                {customer.first_name + " " + 
                                                customer.last_name + ", " + 
                                                customer.email}
                                            </option>
                                        )
                                }  
                                </Form.Control>    
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                                <Form.Control
                                    onChange={(e) => create('weight', e.target.value)}
                                    type="text" placeholder={parcel.weight}>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                                <Form.Control as="select" 
                                    onChange={(e) => create('categories_id', e.target.value)}
                                    type="select" >
                                    <option value="0">{parcel.category}</option>
                                    {categories
                                    .map(category => 
                                                <option value={category.id} key={category.id}>
                                                    {category.category}
                                                </option>)}  
                                </Form.Control>    
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                                <Form.Control as="select" 
                                    onChange={(e) => {
                                        create('warehouses_id', e.target.value);
                                        setWarehouseId(e.target.value);
                                    }}
                                    type="select" >
                                    <option value="0">{parcel.warehouse}</option>
                                    {warehouses
                                        .map(wh => 
                                                <option value={wh.id} key={wh.id} name={wh.warehouse}>
                                                    {wh.warehouse + ", " + 
                                                    wh.street_name + ", " + 
                                                    wh.city + ", " + 
                                                    wh.country}
                                                </option>
                                            )
                                    }  
                                </Form.Control>    
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                                <Form.Control as="select" 
                                    onChange={(e) => create('shipments_id', e.target.value)}
                                    type="select" >
                                <option value="0">
                                    {   updatedParcel.shipments_id === parcel.shipments_id
                                        ? parcel.shipment
                                        : ''
                                    }
                                </option>
                                {shipments
                                    .filter(shipment => shipment.status === 'preparing')
                                    .filter(shipment => shipment.delivery_wh_id === +warehouseId)
                                    .map(shipment => 
                                            <option value={shipment.id} key={shipment.id}>
                                                {shipment.origin_wh + ", " + 
                                                shipment.delivery_wh + ", " + 
                                                new Date(shipment.departure_date).toLocaleDateString('fr-CA')  + ", " + 
                                                new Date(shipment.arrival_date).toLocaleDateString('fr-CA')  + ", " + 
                                                shipment.status}
                                            </option>   
                                        )
                                }  

                                </Form.Control>    
                            </Form.Group>
                            
                            <Col>{updatedParcel.status}</Col>
                            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                                <Form.Control as="select" 
                                    onChange={(e) => create('delivered_to_address', e.target.value)}
                                    type="select" >
                                <option>{ parcel.delivered_to_address === 1 ? 'to address' : 'pick up' }</option>
                                <option value="0">pick up</option>
                                <option value="1">to address</option>  
                                </Form.Control>    
                            </Form.Group>
                        </Row>           
                    </Card.Text>
                    </Card.Body>
                </Card>
                : <SingleParcel parcel={parcel} />
            }
            {
                editMode
                ? <Button variant='primary' style={{margin: '1rem'}} 
                    onClick={e => {
                        setEditMode(!editMode)
                        }
                    }>
                        Cancel
                </Button>
                : <Link to="/parcels" >
                    <Button variant='primary' style={{margin: '1rem'}}>
                        Cancel
                    </Button> 
                </Link>
            }
            {
                editMode
                ? <Button variant='primary' style={{margin: '1rem'}} 
                    onClick={e => {
                        sendParcel(updatedParcel)
                        setEditMode(!editMode)
                        }
                    }>
                    Update
                </Button>
                : <Button variant='primary' style={{margin: '1rem'}} onClick={e => setEditMode(!editMode)} >
                    Edit
                </Button>
            }
        </Form>
        </div>
    );
};

export default Parcel;
