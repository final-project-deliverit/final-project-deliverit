import { useEffect, useState } from "react";
import { Button, Form, Row, Col } from 'react-bootstrap';
import { createAddress, getAllCountries, getCitiesByCountry } from "../../../../requests/AddressRequest";
import { createNewWh, getWarehouseById } from "../../../../requests/WarehousesRequests";
import Address from "../../../Address/Address";
import VerticalNav from "../../../VerticalNav/VerticalNav";
import { useHistory } from "react-router-dom";

const CreateWarehouse = () => {

    let history = useHistory();


    // Address
    const [countries, setCountries] = useState([]);
    const [countryId, setCountryId] = useState(null);
    const [cities, setCities] = useState([]);

    useEffect(() => {
        getAllCountries()
        .then(data => setCountries(data))
    }, []);

    useEffect(() => {
        getCitiesByCountry(countryId)
        .then(data => setCities(data))
    }, [countryId]);


    const [ warehouse, setWarehouse ] = useState({
        warehouse: '',
        addresses_id: '',
    });

    const [ address, setAddress ] = useState({
        cities_id: '',
        street_name: '',
    });

    const create = (prop, value) => {
        address[prop] = value;
        setAddress({...address})
    };
    const createWhData = (prop, value) => {
        warehouse[prop] = value;
        setWarehouse({...warehouse})
    };

    const createNewWarehouse = (whData, addressData) => {
        createAddress(addressData)
            .then(res => createWhData('addresses_id', res.id))
            .then(createNewWh(whData).then(res => getWarehouseById(res.id)).then(res => history.push({pathname: "/warehouses", state: res})))
        
        // createNewWh(data)
        //     .then(res => getWarehouseById(res.id))
        //     .then(res => setWarehouses([...warehouses, ...res]))
        //     .then(history.push("/warehouses"))
    };

    const labelStyle = { fontSize: '20px', color: 'black', marginLeft: '10px', display: 'flex' };

    //

    return ( 
        <div>
            <VerticalNav />
            <div style={{ width: '55%', marginBlock: '30px', marginInline: 'auto' }}>
                <h3 style={{ fontFamily: 'cursive', color: '#4042e2', fontWeight: '600'}}>Create new warehouse</h3>
                <Form.Group as={Col} className="wh" controlId="formCreateWh">
                    <hr/>
                    <Row className="wh-1">
                        <Form.Group as={Col} className="wh-1" controlId="Warehouse">
                            <Form.Label style={ labelStyle }>Warehouse name</Form.Label>
                            <Form.Control maxLength={15} onChange={(e) => {create('warehouse', e.target.value); createWhData('warehouse', e.target.value)}}
                                type="warehouseName" placeholder="..." />
                            <div style = {{ fontSize: '12px', marginTop: '10px', marginBottom: '10px' }}>The name should be between 5-15 symbols</div>
                        </Form.Group>
                    </Row>
                    <Address countries={countries} cities={cities} setCountryId={setCountryId} create={create} />
                </Form.Group>
                <Button style={{margin: '1rem 1rem 1rem 3rem'}} onClick={() => history.push("/warehouses")}>Cancel</Button>
                <Button onClick={() => {createNewWarehouse(warehouse, address); }} >Confirm</Button>
            </div>
        </div>
    );
};

export default CreateWarehouse;