import { useEffect, useState } from 'react';
import { Card, Button, Form } from 'react-bootstrap';
import { getAddressById, getAllCountries, getCitiesByCountry, updateAddress } from '../../../../requests/AddressRequest';
import { getWarehouseById, updateWhData } from '../../../../requests/WarehousesRequests';
import Address from '../../../Address/Address';
import { FaWarehouse } from "react-icons/fa";

const SingleWarehouse = ({ warehouse, setWarehouse, wh}) => {

    const [ whState, setWhState ] = useState(wh);

    const [ editMode, setEditMode ] = useState(false);
    const [ updateWarehouse, setUpdateWarehouse ] = useState({
        id: whState.id,
        warehouse: whState.warehouse,
        addresses_id: whState.addresses_id
    });

    //Address logic start

    const [countries, setCountries] = useState([]);
    const [countryId, setCountryId] = useState(0);
    const [cities, setCities] = useState([]);
    const [newAddressId, setNewAddressId] = useState(0);
    const [updatedAddress, setUpdatedAddress] = useState({
        id: whState.addresses_id,
        cities_id: 0,
        street_name: '',
    });

    useEffect(() => {
        getAllCountries()
        .then(data => setCountries(data))
    }, []);

    useEffect(() => {
        getCitiesByCountry(countryId)
        .then(data => setCities(data))
    }, [countryId]);

    useEffect(() => {
        getAddressById(newAddressId)
        .then(data => data[0])
    }, [newAddressId])
    

    const create = (prop, value) => {
        updatedAddress[prop] = value;
        setUpdatedAddress({ ...updatedAddress })
    };

    const warehouseDataUpdate = (prop, value) => {
        updateWarehouse[prop] = value;
        setUpdateWarehouse({ ...updateWarehouse })
    };

    ///////////// Address logic end

    const changeWarehouseData = () => {

        updateAddress(updatedAddress)
            .then((res) => updateWhData(updateWarehouse))
            .then(res => getWarehouseById(res.id))
            .then(res => setWhState(...res))
    };

    return (
        <div>
                    <div className="singleWarehouse">
                        <h3>Warehouse: {whState.warehouse}</h3>
                        <span>
                        <FaWarehouse />
                        </span>
                        <h1>Location</h1>
                        <h2>{whState.street_name + ", " + whState.city + ", " + whState.country}</h2>
                        <button className="edit-btn" onClick={() => { setEditMode(true) }}>Edit</button>
                    </div>

                { editMode &&  
                <div style={{ width: '100%', height: '100vh', backgroundColor: "rgba(0, 0, 0, 0.5)", position: 'fixed', top: '0', left: '0'}}>
                <Card border="primary" style={{ width: '28rem', marginBottom: '10px', marginTop: '15px', position: 'fixed', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}}>
                    <Card.Header ><span style={{ fontStyle: "oblique", fontFamily: "cursive" }}>Warehouse</span> 
                    <textarea type="text" style={{ height: '35px', width: '150px', marginLeft: '20px'}} value={updateWarehouse.warehouse} onChange={(e) => { warehouseDataUpdate('warehouse', e.target.value) }}></textarea>
                    </Card.Header>
                    <Card.Body>
                    <Card.Title>Choose Warehouse Location</Card.Title>
                    <Card.Text>
                        <Address 
                            setUpdateWarehouse={setUpdateWarehouse} setCountryId={setCountryId}
                            countries={countries} create={create}
                            cities={cities} />
                    </Card.Text>       
                    <Button variant='danger' onClick={() => setEditMode(false) } > Cancel</Button>&nbsp;
                    <Button variant='success' onClick={() => { changeWarehouseData(); setEditMode(false) }} > Submit</Button>
                    </Card.Body>
                </Card>
                </div> }
            
        </div>

    );
};

export default SingleWarehouse;