import { useEffect, useState } from "react";
import './Warehouses.css';
import { getAllWarehouses } from "../../../../requests/WarehousesRequests";
import SingleWarehouse from "./SingleWarehouse";
import CreateWarehouse from "./CreateWarehouse";
import VerticalNav from './../../../VerticalNav/VerticalNav';
import { Link, useLocation } from 'react-router-dom';
import { Button } from 'react-bootstrap';


const Warehouses = () => {

    const [ warehouses, setWarehouses ] = useState([]);
    const [ createWarehouses, setCreateWarehouses ] = useState(false);

    const { state } = useLocation();
    console.log(state);
    useEffect(() => {
        getAllWarehouses()
            .then(data => setWarehouses(data))
    }, [])

    return (
            <div>
                <VerticalNav />
        <div className="warehouses-page">
            <div className="upper-side">
                <div className="switch">
                    <h2>All Warehouses</h2>
                    <Link to="/warehouses/create">
                        <Button variant='dark' style={{ fontSize: '20px', color: '#fff', backgroundColor: '#4042e2', border: 0, textAlign: 'center' }}>
                            Create Warehouse
                        </Button>
                    </Link>
                </div> 
            </div> 
            <div className="container-warehouses">{warehouses.sort((a, b) => b.id - a.id).map(wh => 
                    <SingleWarehouse key={wh.id} warehouses={warehouses} setWarehouses={setWarehouses} wh={wh}/>  
            )}
            </div>
        </div>
        </div> 

    );
};

export default Warehouses;