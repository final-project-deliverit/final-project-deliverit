import { useEffect, useState } from 'react';
import { Card, Form, Col, Button } from 'react-bootstrap';
import { createShipment, getShipmentById } from '../../../../requests/ShipmentsRequests';
import { getAllWarehouses } from '../../../../requests/WarehousesRequests';
// import Shipments from './Shipments';
import { useHistory } from "react-router-dom";
import VerticalNav from '../../../VerticalNav/VerticalNav';

const CreateShipment = ({ shipments, setShipments }) => {
    let history = useHistory();

    const [ warehouses, setWarehouses ] = useState([]);
    const [ shipment, setShipment ] = useState({
        origin_wh_id: '',
        delivery_wh_id: '',
        departure_date: '',
        arrival_date: ''
    });

    const create = (prop, value) => {
        shipment[prop] = value;
        setShipment({...shipment})
    };
    useEffect(() => {
        getAllWarehouses()
            .then(data => setWarehouses(data))
    }, []);

    const addShipment = () => {
        createShipment(shipment)
            .then(res => getShipmentById(res.id))
            .then(res => history.push({pathname: "/shipments", state: res}))
    };

    const labelStyle = { fontSize: '20px', color: 'black', marginLeft: '10px', display: 'flex', fontWeight: '600', padding: '10px' };

    return (
        <div>
            <VerticalNav />
        <div>
            <Form style={{ width: '55%', marginBlock: '30px', marginInline: 'auto' }}>
            <h3 style={{ fontFamily: 'cursive', color: '#4042e2', fontWeight: '600'}}>Create new shipment</h3>
            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ {...labelStyle, marginTop: '20px'} }>Departure warehouse</Form.Label>
                <Form.Control as="select" 
                onChange={(e) => create('origin_wh_id', e.target.value)}
                type="select" >
                <option value="0">Warehouse...</option>
                {warehouses.map(wh => <option value={wh.id} key={wh.id}>{wh.warehouse + ", " + wh.street_name + ", " + wh.city + ", " + wh.country}</option>)}
                </Form.Control>
            </Form.Group>
            <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                <Form.Label style={ labelStyle }>Arrival warehouse</Form.Label>
                <Form.Control as="select" 
                onChange={(e) => create('delivery_wh_id', e.target.value)}
                type="select" >
                <option value="0">Warehouse...</option>
                {warehouses.map(wh => <option value={wh.id} key={wh.id}>{wh.warehouse + ", " + wh.street_name + ", " + wh.city + ", " + wh.country}</option>)}  
                </Form.Control>    
            </Form.Group>
            <Card.Text style={{ marginLeft:'15px', marginTop: '20px'}}>
                <label for="meeting-time">Set departure date and time:</label>&nbsp;&nbsp;
                <input type="datetime-local" id="meeting-time"
                name="meeting-time" value={shipment.departure_date} onChange={(e) => create('departure_date', e.target.value)}
                min="2021-06-07T00:00" max="2022-12-31T00:00">
                </input>
            </Card.Text>
            <Card.Text style={{ marginLeft:'15px', marginTop: '20px'}}>
                <label for="meeting-time">Set arrival date and time:</label>&nbsp;&nbsp;

                <input type="datetime-local" id="meeting-time"
                name="meeting-time" value={shipment.arrival_date} onChange={(e) => create('arrival_date', e.target.value)}
                min="2021-06-07T00:00" max="2022-12-31T00:00">
                </input>
            </Card.Text>
            <Button style={{ marginLeft:'15px', marginTop: '20px'}} onClick={() => history.push("/shipments")} >Cancel</Button>
            <Button style={{ marginLeft:'15px', marginTop: '20px'}} onClick={() => addShipment()} >Confirm</Button>
            </Form>
        </div>
        </div>
    );
};

export default CreateShipment;