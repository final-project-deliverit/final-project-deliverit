import { useEffect, useState } from "react";
import { getAllShipments, getShipmentsByStatus, getShipmentsByWarehouse } from "../../../../requests/ShipmentsRequests";
import SingleShipment from "./SingleShipment";
import { Button, Col } from 'react-bootstrap';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import VerticalNav from './../../../VerticalNav/VerticalNav';
import Form from 'react-bootstrap/Form';
import STATUS from "../../../../common/Statuses";
import './Shipment.css';
import { Link, useLocation } from "react-router-dom";


const Shipments = () => {

    const [ shipments, setShipments ] = useState([]);
    const [ showShipmentByStatus, setShowShipmentByStatus ] = useState(false);
    const [ showShipmentByWh, setShowShipmentByWh ] = useState(false);

    useEffect(() => {
        getAllShipments()
            .then(res => setShipments(res))
    }, []);

    const renderByStatus = (status_id) => {
        getShipmentsByStatus(status_id)
            .then(res => setShipments(res))
    };

    const renderByWh = (column_name) => {
        getShipmentsByWarehouse(column_name)
            .then(res => setShipments(res))
    };

    const filterControl = (value) => {
        if(value === '1') {setShowShipmentByStatus(true); setShowShipmentByWh(false)}
        else if (value === '2') {setShowShipmentByWh(true); setShowShipmentByStatus(false);}
       
    };

    const { state } = useLocation();
    console.log(state);

    return (
        <div>
            <VerticalNav />
        <div>
            <div className="filter-container">
            <h2>Shipments</h2>
            <div className="inner-filter">
        <Form.Group controlId="formGridState">
            <Form.Control as="select" 
            onChange={(e) => filterControl(e.target.value)}
            type="select" >
                <option value="0">Filter By...</option>
                <option value="1">Status</option>
                <option value="2">Warehouse</option>
            </Form.Control>
        </Form.Group>
        { showShipmentByStatus ? 
        <ButtonGroup aria-label="Basic example">
            <Button className="btn-status" onClick={() => renderByStatus(STATUS.PREPARING) } >Preparing</Button>
            <Button className="btn-status" onClick={() => renderByStatus(STATUS.ON_THE_WAY)} >On the way</Button>
            <Button className="btn-status" onClick={() => renderByStatus(STATUS.COMPLETED)} >Completed</Button>
        </ButtonGroup> 
        : null}
        { showShipmentByWh ? 
        <ButtonGroup aria-label="Basic example">
            <Button className="btn-status" onClick={() => renderByWh('origin_wh')} >Origin warehouse</Button>
            <Button className="btn-status" onClick={() => renderByWh('delivery_wh')} >Delivery warehouse</Button>
        </ButtonGroup> 
        : null}
        </div>
        <Link to="/shipments/create">
            <Button variant='success' > Create Shipment</Button> 
        </Link>
        </div>
        <div className="shipments-container">
            <div className="Switch">
                {shipments.map(el =>  
                    <SingleShipment key={el.id} el={el} shipments={shipments} setShipments={setShipments} />
                )}
            </div>
        </div>
        </div>
        </div>
    );
};

export default Shipments;