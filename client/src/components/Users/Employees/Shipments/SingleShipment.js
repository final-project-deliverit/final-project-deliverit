import './Shipment.css';
import { Card, Button, Form, ListGroup, Col, Row} from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { getParcelById, getParcelsByShipment, getParcelsByStatus, updateShipmentId } from '../../../../requests/ParcelsRequests';
import { getAllWarehouses } from '../../../../requests/WarehousesRequests';
import { getShipmentById, updateShipment } from '../../../../requests/ShipmentsRequests';
import { Link } from 'react-router-dom';

const SingleShipment = ({ el, shipments, setShipments }) => {

    const id = el.id;
    // console.log(el);

    const [ sh ,setSh ] = useState(el)

    const [ parcels, setParcels ] = useState([]);
    const [ editMode, setEditMode ] = useState(false);
    useEffect(() => {
        getParcelsByShipment(id)
            .then(res => setParcels(res))
    }, [id]);

    const labelStyle = { fontSize: '20px', color: 'black', marginLeft: '10px', display: 'flex', fontWeight: 'bold', padding: '0 0 10px 0px' };

    //Edit mode

    const [ warehouses, setWarehouses ] = useState([]);

    const [ shipment, setShipment ] = useState({
        id: id,
        origin_wh_id: '',
        delivery_wh_id: '',
        departure_date: '',
        arrival_date: '',
        status_id: ''
    });

    const create = (prop, value) => {
        shipment[prop] = value;
        setShipment({...shipment})
    };
    useEffect(() => {
        getAllWarehouses()
            .then(data => setWarehouses(data))
    }, []);

    const reworkedShipment = () => {

        updateShipment(shipment)
            .then(res => getShipmentById(res.id))
            .then(res => setSh(...res));
    };

    //Add/remove parcel

    const [ showAddParcel, setShowAddParcel ] = useState(false);
    const [ parcelsWithStatus, setParcelsWithStatus ] = useState([]);
    const [ parcelId, setParcelId ] = useState(null);

    const STATUS = {
        PREPARING: 1
    }
    const loadParcels = () => {

        getParcelsByStatus(STATUS.PREPARING)
            .then(res => setParcelsWithStatus(res));
    };

    const addParcel = () => {

        const obj = {
            id: parcelId,
            ship_id: id
        };

        updateShipmentId(obj)
            .then(res => getParcelById(res.id))
            .then(res => setParcels([...parcels, ...res]))
    };

    return (
        <div className="single-shipment-container">
            <details>
                <summary>
                    <div>
                    <p>Shipment ID: {id}</p>
                    { !showAddParcel? <Button variant='success' onClick={() => {setShowAddParcel(true); loadParcels() }} >Add parcel</Button> : null}
                    </div>
                </summary>
            
            <div className="shipment">
                <ul>
                    <li>
                        <b>Origin warehouse:</b>
                        <p>{sh.origin_wh}</p>
                    </li>
                    <li>
                        <b>Delivery warehouse:</b>
                        <p>{sh.delivery_wh}</p>
                    </li>
                    <li>
                        <b>Departure date:</b>
                        <p> {new Date(sh.departure_date).toLocaleDateString()}</p>
                    </li>
                    <li>
                        <b>Arrival date:</b>
                        <p>{new Date(sh.arrival_date).toLocaleDateString()}</p>
                    </li>
                    <li>
                        <b>Status:</b>
                        <p>{sh.status}</p>
                    </li>
                </ul>
            </div>
            <Button style={{ marginBottom: '15px', marginLeft: '15px'}} variant="secondary" onClick={() => setEditMode(true)} >Edit</Button>
            </details>
            {
            editMode &&
            <div style={{ width: '100%', height: '100vh', backgroundColor: "rgba(0, 0, 0, 0.5)", position: 'fixed', top: '0', left: '0'}}>
            <Card border="primary" style={{ width: '32rem', marginBottom: '10px', marginTop: '15px', position: 'fixed', left: '50%', top: '50%', transform: 'translate(-50%, -50%)'}}>
                <Card.Header>
                <span style={{ fontStyle: "oblique", fontFamily: "cursive" }}>Shipment ID: {id}</span></Card.Header>
                <Card.Body>
                    <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                    <Form.Label style={ labelStyle }>Departure warehouse</Form.Label>
                    <Form.Control as="select" 
                        onChange={(e) => create('origin_wh_id', e.target.value)}
                        type="select" >
                        <option value="0">{sh.origin_wh}</option>
                        {warehouses.map(wh => <option value={wh.id} key={wh.id}>{wh.warehouse + ", " + wh.street_name + ", " + wh.city + ", " + wh.country}</option>)}
                    </Form.Control>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formGridState" style={{ fontSize: '20px'}}>
                    <Form.Label style={ {...labelStyle, padding: '15px 0 10px 0'} }>Arrival warehouse</Form.Label>
                    <Form.Control as="select" 
                        onChange={(e) => create('delivery_wh_id', e.target.value)}
                        type="select" >
                        <option value="0">{sh.delivery_wh}</option>
                        {warehouses.map(wh => <option value={wh.id} key={wh.id}>{wh.warehouse + ", " + wh.street_name + ", " + wh.city + ", " + wh.country}</option>)}
                    </Form.Control>
                    </Form.Group>
                    <Card.Text style={{ marginLeft:'15px', marginTop: '20px'}}>
                        <label for="meeting-time">Set departure date and time:</label>&nbsp;&nbsp;
                        <input type="datetime-local" id="meeting-time"
                        name="meeting-time" value={shipment.departure_date} onChange={(e) => create('departure_date', e.target.value)}
                        min="2021-06-07T00:00" max="2022-12-31T00:00">
                        </input>&nbsp;
                    </Card.Text>
                    <Card.Text style={{ marginLeft:'15px', marginTop: '20px'}}>
                        <label for="meeting-time" >Set arrival date and time:</label>&nbsp;&nbsp;
                        <input type="datetime-local" id="meeting-time"
                        name="meeting-time" value={shipment.arrival_date} onChange={(e) => create('arrival_date', e.target.value)}
                        min="2021-06-07T00:00" max="2022-12-31T00:00">
                        </input>
                    </Card.Text>
                    <Card.Title style={{ marginLeft:'15px'}}>Status</Card.Title>
                    <Card.Text style={{ marginLeft:'15px', marginTop: '20px'}}>
                        <label for="statuses">Choose a status:</label>&nbsp;&nbsp;
                        <select id="statuses" name="statusList" form="status" onChange={(e) => create('status_id', e.target.value)}>
                            <option value="0">-</option>
                            <option value="1">Preparing</option>
                            <option value="2">On the way</option>
                            <option value="3">Completed</option>
                        </select>
                    </Card.Text>
                <Button style={{ marginLeft:'15px'}} variant="secondary" onClick={() => setEditMode(false)} >Cancel</Button> 
                <Button style={{ marginLeft:'15px'}} variant="secondary" onClick={() => {reworkedShipment(); setEditMode(false)}} >Submit</Button>
                </Card.Body>
            </Card>
            </div> 
            }
            <div>
                <ListGroup variant="flush">
                    {parcels.length === 0 ? <p className="parcel-status">There are no parcels with this shipment</p>  
                    : parcels.map(p => 
                        <div key={p.parcel} style={{ display: 'flex', flexDirection: 'row', borderTop: 'solid 2px rgb(124, 123, 123)'}}>
                            <Form.Label style={{ fontSize: '20px', fontWeight: '800', marginTop: '15px', marginLeft: '15px'}} >Parcel ID:</Form.Label>&nbsp;&nbsp;
                            <Form.Text style={{ fontSize: '20px', marginTop: '15px'}} >{p.parcel}</Form.Text>
                            <Form.Label style={{ fontSize: '20px', fontWeight: '800', marginTop: '15px', marginLeft: '15px'}} >Customer:</Form.Label>&nbsp;&nbsp;
                            <Form.Text style={{ fontSize: '20px', marginTop: '15px'}} >{p.costumer }</Form.Text>
                            <Link style={{ fontSize: '20px', marginTop: '15px', marginLeft: '25px', textDecoration: 'none'}} to={`/parcels/parcel/${p.parcel}`} >
                                <span style={{ fontStyle: "oblique", fontFamily: "cursive" }}> &rarr;
                                    more details &nbsp;
                                </span> 
                            </Link>
                        </div>) }
                    
                    { showAddParcel ? 
                    <div style={{ marginLeft:'15px', marginTop: '20px'}}>
                        <label for="parcel" style={{ fontWeight: '700'}}>Choose a parcel to add:</label>&nbsp;&nbsp;
                        <select id="parcel" name="statusList" form="parcel" onChange={(e) => setParcelId(e.target.value)}>
                            {parcelsWithStatus.map(p => <option value={p.parcel} key={p.parcel}>{'Parcel ID: ' + p.parcel + ", Customer: " + p.customer}</option>)}

                        </select>
                        <Button style={{ marginLeft:'45px'}} onClick={() => setShowAddParcel(false)} >Cancel</Button>
                        <Button style={{ marginLeft:'15px'}} onClick={() => { addParcel(); setShowAddParcel(false)}} >Add</Button>
                    </div>
                    : null }
                </ListGroup>
            </div>
        </div>
    );
};

export default SingleShipment;