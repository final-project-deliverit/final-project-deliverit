import { useContext, useEffect, useState } from 'react';
import { getParcelsByCustomer } from './../../../requests/ParcelsRequests';
import AuthContext from "../../../providers/AuthContext";
import { Button } from 'react-bootstrap';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import STATUS from './../../../common/Statuses';
import ParcelCustomer from './ParcelCustomer';


const MyOrders = () => {

    const { user } = useContext(AuthContext);
    const [ parcels, setParcels ] = useState([]);
    const [ parcelByStatus, setParcelByStatus] = useState([]);

    const id = user.id;

    const filterParcels = (status_id) => {
        const p = status_id === 0 ? parcels : parcels.filter(el => el.status_id === status_id);
        
        return (p.length !== 0) ? p : [{ msg: 'You have no parcels with this status' }];
    };

    useEffect(() => {
        getParcelsByCustomer(id)
            .then(data => setParcels(data));
    }, [id]);


    return (
            <div className="filter-container">
                <div className="Switch">
                    <div className="parcels-header">
                        <h2>Parcels</h2>

                        <ButtonGroup aria-label="Basic example">
                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(0))} >All Parcels</Button>
                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(STATUS.PREPARING)) } >Preparing</Button>
                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(STATUS.ON_THE_WAY))} >On the way</Button>

                            <Button className="btn-status" onClick={() => setParcelByStatus(filterParcels(STATUS.COMPLETED))} >Completed</Button>
                        </ButtonGroup> 
                    </div>
                             
                    <div>
                        { parcelByStatus.length === 0 ? 
                        parcels.map(p => 
                            <ParcelCustomer parcel={p} />) 
                        : null}
                        {parcelByStatus.map(parcel => 
                            <div key={parcel.parcel}>
                                { parcel.parcel > 0
                                    ? <ParcelCustomer key={parcel.parcel} parcel={parcel} /> 
                                    : <p>{parcel.msg}</p>
                                } 
                            </div>)
                        }
                    </div>
                </div>  
            </div>
    );
};

export default MyOrders;