import { useContext, useEffect, useState } from "react";
import AuthContext from "../../../providers/AuthContext";
import { getCustomerById } from "../../../requests/UsersRequests";
import './UserInfo.css'

const UserInfo = () => {

    const { user } = useContext(AuthContext);
    const [ userInfo, setUserInfo ] = useState([]);

    const id = user.id
    useEffect(() => {
        getCustomerById(id)
            .then(data => setUserInfo(...data))
    }, [id]);

    return (
            <div className='user-info-container-2' >
                <div className='user-info-down'>
                    <h1>Account info : </h1> <br/>
                    <div> 
                        <p><b>Username:</b>{' ' + userInfo.username}</p>
                        <p><b>First Name:</b>{' ' + userInfo.first_name}</p>
                        <p><b>Last Name:</b>{' ' + userInfo.last_name}</p>
                        <p><b>Country:</b>{' ' + userInfo.country}</p>
                        <p><b>City:</b>{' ' + userInfo.city}</p>
                        <p><b>Street Name:</b>{' ' + userInfo.street_name}</p>
                        <p><b>Email:</b>{' ' + userInfo.email}</p>
                    </div>
                </div>
            </div>
    );
};

export default UserInfo;