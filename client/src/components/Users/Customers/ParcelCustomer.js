import { Card, Row, Col } from 'react-bootstrap';
import { useState } from 'react';
import { updateParcel } from './../../../requests/ParcelsRequests';
import STATUS from './../../../common/Statuses';


const ParcelCustomer = ({ parcel, id }) => {
    const [ updatedParcel, setUpdatedParcel ] = useState({
        id: id,
        weight: parcel.weight,
        warehouses_id: parcel.warehouses_id,
        categories_id: parcel.categories_id,
        shipments_id: parcel.shipments_id,
        users_id: parcel.users_id,
        delivered_to_address: parcel.delivered_to_address,
    });

    const [ checked, setChecked ] = useState(updatedParcel.delivered_to_address === 0 ? true : false);


    console.log('updatedParcel', updatedParcel);

    const create = (prop, value) => {
        updatedParcel[prop] = value;
        setUpdatedParcel({ ...updatedParcel })
    };


    const change = (value, parcel) => {
        create('delivered_to_address', value);
        updateParcel(parcel)
        .then(res => setChecked(!checked));
    }

    console.log(parcel);


    return (
        <Card border="primary" style={{ width: '70rem', marginBottom: '10px', marginTop: '15px'}}>
            <Card.Header >
                    <span style={{ fontStyle: "oblique", fontFamily: "cursive" }}> &nbsp;
                        Order &nbsp;
                    </span> 
                    {parcel.parcel}
            </Card.Header>
            <Card.Body>
                <Card.Title>
                    <Row>
                        <Col>costumer  </Col>
                        <Col>weight    </Col>
                        <Col>category  </Col>
                        <Col>warehouse </Col>
                        <Col>status  </Col>
                        <Col>delivery</Col>
                    </Row>  
                </Card.Title>
                <Card.Text>
                    <Row>
                        <Col>{parcel.costumer}</Col>
                        <Col>{parcel.weight} kg</Col>
                        <Col>{parcel.category}</Col>
                        <Col>{parcel.warehouse}</Col>
                        <Col>{Object.keys(STATUS)[parcel.status_id - 1]}</Col>
                        <Col>
                            <input onChange={(e) => change(e.target.value, updatedParcel)}
                                type="radio" 
                                value={0} 
                                checked={checked} 
                            /> pick up 
                            <br/>
                            <input onChange={(e) => change(e.target.value, updatedParcel)}
                                type="radio" 
                                value={1}  
                                checked={!checked} 
                            /> to address
                        </Col>
                    </Row>           
                </Card.Text>
            </Card.Body>
        </Card>
    );
};

export default ParcelCustomer;