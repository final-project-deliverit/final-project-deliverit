import { Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import './VerticalNav.css'

const VerticalNav = () => {

    return (
    <Nav defaultActiveKey="/home" className="container-vn">
        <Nav.Link >
            <Link to='/warehouses' className="links-vn" >
                Warehouses
            </Link>
        </Nav.Link>
        <Nav.Link >
            <Link to='/shipments' className="links-vn" >
                Shipments
            </Link>
        </Nav.Link>
        <Nav.Link >
            <Link to='/parcels' className="links-vn" >
                Parcels
            </Link>
        </Nav.Link>
        <Nav.Link >
            <Link to='/users/info' className="links-vn">
                Users
            </Link>
        </Nav.Link>
    </Nav>
    )
};

export default VerticalNav;