import { useState } from 'react';
import { Button, Form, Row, Col } from 'react-bootstrap';

const Address = ({ setUpdateWarehouse, setCountryId, countries, create, cities }) => {

    const labelStyle = { fontSize: '15px', color: 'black', marginLeft: '10px', display: 'flex' };

    const [minStreetLength, setMinStreetLength] = useState(false); 

return (
<div>
    <Form.Group as={Col} controlId="formGridState">
         <Form.Label style={ labelStyle }>Country</Form.Label>
            <Form.Control as="select" 
            onChange={(e) => setCountryId(e.target.value)}
            type="select" >
            <option value="0">Country...</option>
            {countries.map(country => <option value={country.id} key={country.id}>{country.country}</option>)}
            </Form.Control>
    </Form.Group>
    <Form.Group as={Col} className="mb-3" controlId="formBasicLastName">
    <Form.Label style={ labelStyle }>City</Form.Label>
    <Form.Control as="select" 
        onChange={(e) => create('cities_id', e.target.value)}
        type="select" >
        <option value="0">City...</option>
        {cities.length > 0 && cities.map(city => <option value={city.id} key={city.id}>{city.city}</option>)}
    </Form.Control>
    </Form.Group>
    <Form.Group as={Col} className="mb-3" controlId="formBasicLastName">
            <Form.Label style={ labelStyle }>Street</Form.Label>
            <Form.Control maxLength={20} 
                onChange={(e) => {
                    if (e.target.value.length > 5) {
                        setMinStreetLength(true);
                    }
                    else {
                        setMinStreetLength(false);
                    }
                    create('street_name', e.target.value)
                }}
                type="street" placeholder="Street" />
    </Form.Group>
</div>
)};

export default Address;