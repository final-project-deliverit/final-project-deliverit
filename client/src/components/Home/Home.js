import { useContext, useEffect, useState } from 'react';
import AuthContext from '../../providers/AuthContext';
import { getAllCustomers } from '../../requests/UsersRequests';
import PublicWarehouses from './../Users/Public/PublicWarehouses';
import './Home.css';
import hero from "../../media/hero.png";

const HomePage = () => {

    const { user } = useContext(AuthContext);
    const [customers, setCustomers ] = useState([]);

    useEffect(() => {
        getAllCustomers()
            .then(data => setCustomers(data))
    }, [])


    return (
        <div>
        <div className="main">
        <div className="left">
            <h1 className="header-hero">
            We made
            <br />
            <span> transportation easy!</span>
            </h1>
            <h2>
            Trusted by <span style={{ color: "#000" }}> {customers.length}+ </span> clients
            </h2>
            <a href="#warehouse">View all warehouses</a>
        </div>
        <div className="right">
            <img src={hero} alt="Hero banner" />
        </div>
        </div>
        <PublicWarehouses /> 
    </div>
    );
    
};
export default HomePage;