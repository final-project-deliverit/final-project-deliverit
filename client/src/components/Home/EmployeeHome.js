import { useContext, useEffect, useState } from 'react';
import './EmployeeHome.css';
import STATUS from '../../common/Statuses';
import AuthContext from '../../providers/AuthContext';
import VerticalNav from './../VerticalNav/VerticalNav';
import { getShipmentsByStatus } from './../../requests/ShipmentsRequests';
import { Card, Row, Col } from 'react-bootstrap';
import { getAllWarehouses } from '../../requests/WarehousesRequests';
import { Form } from 'react-bootstrap';


const EmployeeHomePage = () => {

    const { user } = useContext(AuthContext);

    const [ shipmentsOnTheWay, setShipmentsOnTheWay ] = useState([]);
    const [ warehouses, setWarehouses ] = useState([]);
    const [ warehousesId, setWarehousesId ] = useState(0);


    useEffect(() => {
        getShipmentsByStatus(STATUS.ON_THE_WAY)
            .then(res => setShipmentsOnTheWay(res))
    }, []);

    useEffect(() => {
        getAllWarehouses()
            .then(data => setWarehouses(data))
    }, []);

    const shipmentsOnAWayToWh = shipmentsOnTheWay
                                .filter(shipment => shipment.delivery_wh_id === +warehousesId)
                                .sort(( a, b ) => {
                                    if ( a.arrival_date < b.arrival_date ){
                                      return -1;
                                    }
                                    if ( a.arrival_date > b.arrival_date ){
                                      return 1;
                                    }
                                    return 0;
                                  });

    console.log(shipmentsOnTheWay);
    console.log(warehousesId);


    return (
        <div>
            <VerticalNav />
            <Card border="primary">
            
            <div className="upper-home-employee">
                <Card.Header>
                <div className="up-employee">
                    <h5>The next shipment that arrives at warehouse</h5>
                    <Form.Group className="employee-form">
                        <Form.Control
                            as="select"
                            onChange={(e) => setWarehousesId(e.target.value)}
                            type="select"
                        >
                        <option value="0">Warehouse...</option>
                        {warehouses.map(warehouse => (
                            <option value={warehouse.id} key={warehouse.id}>
                            {warehouse.warehouse}
                            </option>
                        ))}
                        </Form.Control>
                    </Form.Group>
                </div>
                </Card.Header>
                <Card.Body>
                <Form>
                    <Col>
                        <Form.Group>
                        {shipmentsOnTheWay.length > 0 &&
                            shipmentsOnAWayToWh.length > 0
                            ? shipmentsOnAWayToWh.map(shipment => (
                            <div key={shipment.id}>
                                <p> <b>{`Shipment: `}</b> {shipment.id}</p> 
                                <p> <b>{`Status: `}</b> {shipment.status}</p>
                                <p><b>{`Depart from: `}</b> {shipment.origin_wh} on  {new Date(shipment.departure_date).toLocaleDateString()}</p> 
                                <p><b>{`Arrive to: `}</b> {shipment.delivery_wh} on  {new Date(shipment.arrival_date).toLocaleDateString()}</p> 
                            </div>
                            ))[0]
                            :  +warehousesId !== 0  && 
                                <div> <>No Shipments On a way to</> &nbsp;
                                    {warehouses.map(warehouse => (
                                        <>
                                            {warehouse.id === +warehousesId && warehouse.warehouse}
                                        </>
                                    ))}
                                </div>
                            }
                        </Form.Group>
                    </Col>
                </Form>
                </Card.Body>
            </div>
            </Card>
            <div className='down-employee'>
                There are <b>{shipmentsOnTheWay.length}</b> shipments on the way! 
            </div>
            <div className="home-employee">
                { shipmentsOnTheWay.map(sh =>  
                    <div className="shipment-employee">
                        <Card border="primary">
                            <Card.Header>
                                Shipment ID: {sh.id}
                            </Card.Header>
                            <Card.Body>
                            <Row>
                                <Col>
                                    <Card.Title>Origin warehouse</Card.Title>
                                </Col>
                                <Col>
                                    <Card.Text>
                                        {sh.origin_wh}
                                    </Card.Text>
                                </Col>
                                <Col>
                                    <Card.Title>Delivery warehouse</Card.Title>
                                </Col>
                                <Col>
                                    <Card.Text>
                                        {sh.delivery_wh}
                                    </Card.Text>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Card.Title>Departure date</Card.Title>
                                </Col>
                                <Col>
                                    <Card.Text>
                                        {new Date(sh.departure_date).toLocaleDateString()}
                                    </Card.Text>
                                </Col>
                                <Col>
                                    <Card.Title>Arrival date</Card.Title>
                                </Col>
                                <Col>
                                    <Card.Text>
                                        {new Date(sh.arrival_date).toLocaleDateString()}
                                    </Card.Text>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Card.Title>Status</Card.Title>
                                </Col>
                                <Col>
                                    <Card.Text>
                                        {sh.status}
                                    </Card.Text>
                                </Col>
                                <Col></Col>
                                <Col></Col>
                            </Row>
                            </Card.Body>
                        </Card>
                    </div>  
                )}             
            </div>
        </div>
    );
};
export default EmployeeHomePage;