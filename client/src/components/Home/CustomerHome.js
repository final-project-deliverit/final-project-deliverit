import { useContext, useEffect, useState } from 'react';
import './CustomerHome.css';
import STATUS from "../../common/Statuses";
import AuthContext from '../../providers/AuthContext';
import { getParcelsByCustomer } from './../../requests/ParcelsRequests';
import ParcelCustomer from './../Users/Customers/ParcelCustomer';


const CustomerHomePage = () => {

    const { user } = useContext(AuthContext);
    const [ parcels, setParcels ] = useState([]);
    const id = user.id;
    useEffect(() => {
        getParcelsByCustomer(id)
            .then(data => setParcels(data));
    }, [id]);


    return (
        <div className="Home">
            <div className="Switch">
                <h2>Undelivered orders</h2>
                {
                    parcels.length !== 0 
                    ? parcels
                        .filter(el => el.status_id === STATUS.ON_THE_WAY || el.status_id === STATUS.PREPARING)
                        .map(el => 
                        <ParcelCustomer parcel={el} key={el.parcel} id={el.parcel} />
                        )
                    : <div>  
                        You have no undelivered orders! 
                    </div>
                }
            </div>
        </div>
    );


};
export default CustomerHomePage;