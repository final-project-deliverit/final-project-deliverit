import VerticalNav from "../VerticalNav/VerticalNav";
import { useEffect, useState } from "react";
import { getUsers } from "../../requests/UsersRequests";
import { Card, Row, Col } from 'react-bootstrap';

const UsersPage = () => {

    const [ allUsers, setAllUsers ] = useState([]);

    useEffect(() => {
        getUsers('')
            .then(res => setAllUsers(res))
    }, [])

    return (
            <div>
                <VerticalNav />
        <div className="filter-container">
            
            <div>
            <h2>All users</h2>
                {allUsers.map(u => 
                    <Card border="primary" style={{ width: '55rem', marginBottom: '10px', marginTop: '15px'}}>
                    <Card.Header >
                        {/* <Link to={`/parcels/parcel/${parcel.parcel}`} > */}
                            <span style={{ fontStyle: "oblique", fontFamily: "cursive" }}>Username: &nbsp;
                            {u.username}   &nbsp;
                            </span> 
                        {/* </Link> */}
                    </Card.Header>
                    <Card.Body>
                        <Card.Title>
                            <Row>
                                <Col>First Name  </Col>
                                <Col>LastName  </Col>
                                <Col>email  </Col>
                                <Col>Address </Col>
                            </Row>  
                        </Card.Title>
                        <Card.Text>
                            <Row>
                                <Col>{u.first_name}</Col>
                                <Col>{u.last_name} </Col>
                                <Col>{u.email}</Col>
                                <Col>{u.street_name + ", " + u.city + ", " + u.country}</Col>
                            </Row>           
                        </Card.Text>
                    </Card.Body>
                    </Card>)
                }
            </div>    
        </div>
        </div>
    );
};

export default UsersPage;