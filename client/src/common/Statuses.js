const STATUS = {
    PREPARING : 1,
    ON_THE_WAY : 2,
    COMPLETED : 3
}

export default STATUS;