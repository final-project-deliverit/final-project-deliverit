import pool from './pool.js';

const getAllCountries = async() => {
    // eslint-disable-next-line quotes
    const sql = `select * from countries`;

    return await pool.query(sql);
};

export default {
    getAllCountries,
};
