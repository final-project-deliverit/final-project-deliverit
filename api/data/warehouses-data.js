import pool from './pool.js';

const getWarehouses = async () => {
    const sql = `select w.id, w.warehouse, a.street_name, c.city, co.country, w.addresses_id
    from warehouses as w
    join cities as c
    join countries as co
    join addresses as a 
    where w.addresses_id = a.id and c.id = a.cities_id and c.countries_id = co.id
    and w.is_deleted = 0`;

    return await pool.query(sql);
};

const createWarehouse = async (whData) => {

    const { addresses_id, warehouse } = whData;

    const sql = `insert into warehouses (addresses_id, warehouse)
    values (?,?)`;

    const result = await pool.query(sql, [addresses_id, warehouse]);

    return {
        id: result.insertId,
    };
};

const getById = async (id) => {
    console.log(id);
    const sql = `select w.id, w.warehouse, a.street_name, c.city, co.country, w.addresses_id
    from warehouses as w
    join cities as c
    join countries as co
    join addresses as a 
    where w.addresses_id = a.id and c.id = a.cities_id and c.countries_id = co.id
    and w.is_deleted = 0 and w.id = ?`;

    return await pool.query(sql, [id]);
};

const updateWarehouse = async (data) => {

    const { id, warehouse, addresses_id } = data;

    const result = await pool.query(`update warehouses set
    warehouse = ?,
    addresses_id = ?
    where id =?`, [warehouse, addresses_id, id]);

    return {
        id: id
    };
};

const deleteWarehouse = async (data) => {

    const { id } = data;
    
    const deleted = await pool.query(`update warehouses set 
    is_deleted = 1 where id = ?`, id);

    return deleted;
};

export default {
    getWarehouses,
    createWarehouse,
    getById,
    updateWarehouse,
    deleteWarehouse,
};