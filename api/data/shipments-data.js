import pool from './pool.js';

const getShipments = async () => {
    const sql = `select s.id, s.departure_date, s.arrival_date, st.status, w.warehouse as delivery_wh, s.delivery_wh_id, wh.warehouse as origin_wh,
                        s.status_id, s.origin_wh_id
    from shipments as s
    join warehouses as w
    join warehouses as wh
    join statuses as st
    where s.delivery_wh_id = w.id and s.origin_wh_id = wh.id and s.status_id = st.id and s.is_deleted = 0`;

    return await pool.query(sql);
};

const getShipmentById = async (id) => {
    const sql = `select s.id, s.departure_date, s.arrival_date, st.status, w.warehouse as delivery_wh, s.delivery_wh_id, wh.warehouse as origin_wh
    from shipments as s
    join warehouses as w
    join warehouses as wh
    join statuses as st
    where s.delivery_wh_id = w.id and s.origin_wh_id = wh.id and s.status_id = st.id and s.id = ? and s.is_deleted = 0`;

    return await pool.query(sql, id);
};

const getShipmentsWithStatus = async (status_id) => {
    const sql = `select s.id, s.departure_date, s.arrival_date, st.status , w.warehouse as origin_wh, wh.warehouse as delivery_wh,
                s.origin_wh_id, s.delivery_wh_id, s.status_id 
    from shipments as s
    Join warehouses as w
    join warehouses as wh
    join statuses as st
    where status_id = ? 
    and s.origin_wh_id = w.id 
    and s.delivery_wh_id = wh.id 
    and s.status_id = st.id`;

    return await pool.query(sql, status_id);
};

const createShipment = async (data) => {

    const { origin_wh_id, delivery_wh_id, departure_date, arrival_date } = data;

    const result = await pool.query(`insert into shipments (origin_wh_id, delivery_wh_id, departure_date, arrival_date) 
    values (?, ?, ?, ?)`, [origin_wh_id, delivery_wh_id, departure_date, arrival_date]);

    return {
        id: result.insertId,
    };
};

const updateShipment = async (data) => {

    const { id, origin_wh_id, delivery_wh_id, departure_date, arrival_date, status_id } = data;

    const result = await pool.query(`update shipments set
    origin_wh_id = ?,
    delivery_wh_id = ?,
    departure_date = ?,
    arrival_date = ?,
    status_id = ?
    where id = ? and is_deleted = 0` , [origin_wh_id, delivery_wh_id, departure_date, arrival_date, status_id, id]);

    return {
        id: id,
    };
};

const deleteShipment = async (data) => {

    const { id } = data;
    
    const deleted = await pool.query(`update shipments set 
    is_deleted = 1 where id = ?`, id);

    return deleted;
};

const filterByWarehouse = async (column) => {

    const result = await pool.query(`select s.id, s.departure_date, s.arrival_date, st.status , w.warehouse as origin_wh, wh.warehouse as delivery_wh 
    from shipments as s
    Join warehouses as w
    join warehouses as wh
    join statuses as st
    where s.origin_wh_id = w.id 
    and s.delivery_wh_id = wh.id 
    and s.status_id = st.id
    order by ${column} ASC`);

    return result;
};


export default {
    getShipments,
    getShipmentById,
    getShipmentsWithStatus,
    createShipment,
    updateShipment,
    deleteShipment,
    filterByWarehouse,
};