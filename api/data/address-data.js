import pool from './pool.js';

const createAddress = async (data) => {
    const { cities_id, street_name } = data;

    const sql = `
    INSERT INTO addresses(cities_id, street_name)
        VALUES(?, ?)
    `;

    const result = await pool.query(sql, [cities_id, street_name]);

    return {
        id: result.insertId,
        cities_id: cities_id,
        street: street_name,
    };
};

const updateAddress = async (data) => {
    const { id, cities_id, street_name } = data;

    const sql = `
    UPDATE addresses SET 
            cities_id = ?, 
            street_name = ? 
        WHERE id = ? 
    `;

    const result = await pool.query(sql, [cities_id, street_name, id]);

    return {
        id: id,
        cities_id: cities_id,
        street: street_name,
    };
};

const getAddressById = async (id) => {
    const sql = `
    SELECT co.country, ci.city, a.street_name as street
        FROM addresses as a
        JOIN cities as ci
        JOIN countries as co
        WHERE a.cities_id = ci.id
        AND ci.countries_id = co.id
        AND a.id = ?;
    `;

    const result = await pool.query(sql, [id]);

    return result;
};

export default {
    createAddress,
    updateAddress,
    getAddressById,
};
