import mariadb from 'mariadb';
import dotenv from 'dotenv';

const config = dotenv.config().parsed;

const pool = mariadb.createPool({
    
    port: +config.DBPORT,
    database: config.DATABASE,
    user: config.USER,
    password: config.PASSWORD,
    host: config.HOST,
    connectionLimit: 2,

});

export default pool; 