import pool from './pool.js';

const getCategories = async () => {
    const sql = `
    SELECT id, category 
        FROM categories
    `;

    const result = await pool.query(sql);
    
    return { 
        error: null,
        data: [...result],
    };
}; 
export default {
    getCategories,
};
