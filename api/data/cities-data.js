import pool from './pool.js';

const getAllCities = async() => {
    const sql = `
      select ci.id, ci.city, co.country
      from cities as ci
      join countries as co
      where ci.countries_id = co.id`;

    return await pool.query(sql);
};

const getCitiesInCountry = async(country_id) => {
    const sql = `
      select ci.id, ci.city, co.country
      from cities as ci
      join countries as co
      where ci.countries_id = co.id
      and co.id = ?`;

    return await pool.query(sql, [country_id]);
};

export default {
    getAllCities,
    getCitiesInCountry
};