import pool from './pool.js';
// import { isDeleted, isBanned } from '../common/constants.js';

const create = async (username, password, role, address, firstName, lastName, email) => {
    const sql = `
        INSERT INTO users(username, password, roles_id, addresses_id, first_name, last_name, email)
        VALUES (?,?,(SELECT id FROM roles WHERE role = ?),?,?,?,?)
    `;

    const result = await pool.query(sql, [username, password, role, address, firstName, lastName, email]);

    return {
        id: result.insertId,
        username: username
    };
};

const getAllCustomers = async () => {
    const sql = `select u.id, u.first_name, u.last_name, u.email, a.street_name, ci.city, co.country, username 
    from users as u
    join addresses as a
    join cities as ci
    join countries as co on a.id = u.addresses_id and a.cities_id = ci.id and ci.countries_id = co.id 
    where is_deleted = 0 `;

    return await pool.query(sql);
};

const getWithRole = async (username) => {
    const sql = `
    SELECT  u.id, u.username, u.password, roles.role
    FROM users u
    join roles on u.roles_id = roles.id
    where username = ?
    `;

    const result = await pool.query(sql, [username]);

    return result[0];
};

const checkForDuplicateUsername = async (column, value) => {
    const sql = `
    SELECT username
    FROM users
    WHERE ${column}=?`;

    const result = await pool.query(sql, [value]);

    return result[0];
};

const update = async (user) => {
    const { id, username } = user;
    const sql = `
        UPDATE users SET
          username = ?
        WHERE id = ?
        AND is_deleted = 0
    `;

    return await pool.query(sql, [username, id]);
};

const getBy = async (id) => {
    const sql = `
    select u.id, u.first_name, u.last_name, u.email, a.street_name, ci.city, co.country, username
    from users as u
    join addresses as a
    join cities as ci
    join countries as co on a.id = u.addresses_id and a.cities_id = ci.id and ci.countries_id = co.id 
    where u.id =?
`;

    const res = await pool.query(sql, id);
    return res;
};

const searchByName = async (column, value) => {
    
    const sql = `
        SELECT id, email, first_name, last_name
        FROM users
        WHERE ${column[0]} LIKE '%${value}%' or ${column[1]} LIKE '%${value}%'
        AND is_deleted =0
    `;

    return await pool.query(sql);
};

const searchByEmail = async (column, value) => {
    
    const sql = `
        SELECT id, email, first_name, last_name
        FROM users
        WHERE ${column} LIKE '%${value}%' 
        AND is_deleted =0
    `;

    return await pool.query(sql);
};

const deleteCustomer = async (user) => {
    const sql = `UPDATE users 
    SET is_deleted = 1
    WHERE id = ?`;

    const result = await pool.query(sql, user[0].id );

    return result;
}

export default {
    create,
    getAllCustomers,
    getWithRole,
    checkForDuplicateUsername,
    update,
    getBy,
    searchByName,
    searchByEmail,
    deleteCustomer,
};
