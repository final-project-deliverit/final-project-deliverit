import pool from './pool.js';

const getParcels = async () => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS costumer, p.weight, 
            c.category, w.warehouse, p.shipments_id AS shipment, st.status, p.delivered_to_address,
            p.id, p.warehouses_id, p.categories_id, p.users_id, st.id AS status_id
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN users AS u
        JOIN shipments as sh
		JOIN statuses as st
        WHERE p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND p.shipments_id = sh.id
        AND sh.status_id = st.id
        AND p.is_deleted = 0
        ORDER BY p.id DESC
    `;

    return await pool.query(sql);
}; 

const getParcelById = async (id) => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS costumer, p.weight, 
            c.category, w.warehouse, p.shipments_id AS shipment, st.status, p.delivered_to_address,
            p.warehouses_id, p.categories_id, p.shipments_id, p.users_id
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN users AS u
        JOIN shipments as sh
		JOIN statuses as st
        WHERE p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND p.shipments_id = sh.id
        AND sh.status_id = st.id
        AND p.is_deleted = 0
        And p.id = ?
    `;

    return await pool.query(sql, [id]);
}; 

const createParcel = async (data) => {
    const { weight, warehouses_id, categories_id, shipments_id, users_id, delivered_to_address } = data;

    const sql = `
    INSERT INTO parcels(weight, warehouses_id, categories_id, shipments_id, users_id, delivered_to_address)
        VALUES(?, ?, ?, ?, ?, ?)
    `;

    const result = await pool.query(sql, [weight, warehouses_id, categories_id, shipments_id, users_id, delivered_to_address]);

    return {
        id: result.insertId,
        weight: weight,
        warehouses_id: warehouses_id,
        categories_id: categories_id,
        shipments_id: shipments_id,
        users_id: users_id,
        delivered_to_address: delivered_to_address,
    };
};

const updateParcel = async (data) => {
    const { id, weight, warehouses_id, categories_id, shipments_id, users_id, delivered_to_address } = data;

    const sql = `
    UPDATE parcels SET 
            weight = ?, 
            warehouses_id = ?, 
            categories_id = ?, 
            shipments_id = ?, 
            users_id = ?, 
            delivered_to_address = ?
        WHERE id = ? 
        AND is_deleted = 0
    `;

    const result = await pool.query(sql, [weight, warehouses_id, categories_id, shipments_id, users_id, delivered_to_address, id]);

    return {
        id: id,
        weight: weight,
        warehouses_id: warehouses_id,
        categories_id: categories_id,
        shipments_id: shipments_id,
        users_id: users_id,
        delivered_to_address: delivered_to_address,
    };
};

const deleteParcel = async (data) => {
    const { id } = data;

    const sql = `
    UPDATE parcels SET
            is_deleted = 1 
        WHERE id = ?
    `;

    const sqlRes = `
    SELECT *
        FROM parcels
        WHERE id = ?
    `;

    await pool.query(sql, [id]);
    
    const result = pool.query(sqlRes, [id]);

    return result;
};


const filterByWeight = async (minValue, maxValue) => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS costumer, p.weight, 
            c.category, w.warehouse, p.shipments_id AS shipment, p.delivered_to_address  
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN users AS u
        where p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND p.is_deleted = 0 
        AND p.weight >= ? 
        AND p.weight <= ? 
    `;

    const result = await pool.query(sql, [minValue, maxValue]);

    return result;
};

const filterByCustomer = async (value) => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS costumer, p.weight, 
            c.category, w.warehouse, s.status_id, p.delivered_to_address, 
            p.warehouses_id, p.categories_id, p.shipments_id, p.users_id  
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN shipments AS s
        JOIN users AS u
        where p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND s.id = p.shipments_id
        AND p.is_deleted = 0 AND p.users_id = ?
    `;

    const result = await pool.query(sql, value);
    return result;
};

const filterByWarehouse = async (value) => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS costumer, p.weight, 
            c.category, w.warehouse, p.shipments_id AS shipment, p.delivered_to_address  
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN users AS u
        where p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND p.is_deleted = 0 AND p.warehouses_id = ?
    `;

    const result = await pool.query(sql, value);
    return result;
};

const filterByShipment = async (value) => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS costumer, p.weight, 
            c.category, w.warehouse, p.shipments_id AS shipment, p.delivered_to_address  
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN users AS u
        where p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND p.is_deleted = 0 AND p.shipments_id = ?
    `;

    const result = await pool.query(sql, value);
    return result;
};

const filterByCategory = async (value) => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS costumer, p.weight, 
            c.category, w.warehouse, p.shipments_id AS shipment, p.delivered_to_address  
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN users AS u
        where p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND p.is_deleted = 0 AND p.categories_id = ?
    `;

    const result = await pool.query(sql, value);
    return result;
};

const filterByStatus = async (value) => {
    const sql = `
    SELECT p.id AS parcel, concat_ws( ' ' , u.first_name, u.last_name ) AS customer, p.weight, 
        c.category, w.warehouse, p.shipments_id, s.status_id, p.delivered_to_address  
        FROM parcels AS p
        JOIN warehouses AS w
        JOIN categories AS c
        JOIN users AS u
        JOIN shipments as s
        where p.users_id = u.id  
        AND p.categories_id = c.id 
        AND p.warehouses_id = w.id
        AND p.is_deleted = 0 AND s.status_id = ?
    `;

    const result = await pool.query(sql, value);
    return result;
};

const updateShipmentId = async (id, ship_id) => {
    const res = await pool.query(`update parcels 
    set shipments_id = ?
    where id = ? `, [ship_id, id]);

    return {
        id: id
    };
};

export default {
  getParcels,
  getParcelById,
  createParcel,
  updateParcel,
  deleteParcel,
  filterByWeight,
  filterByCustomer,
  filterByWarehouse,
  filterByShipment,
  filterByCategory,
  filterByStatus,
  updateShipmentId
}
