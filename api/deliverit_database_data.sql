-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table deliverit.addresses: ~43 rows (approximately)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`id`, `cities_id`, `street_name`) VALUES
	(1, 4, 'Strada Progresului 12'),
	(2, 4, 'Strada Progresului 10'),
	(3, 12, 'Cavtatska 1'),
	(4, 5, 'Habichtweg 12'),
	(8, 14, 'Carrer de Bailèn 14'),
	(9, 1, 'Obikolna 5'),
	(11, 1, 'Slatinska 49'),
	(12, 3, 'Trakiya 5'),
	(14, 2, 'Kraymorska 124'),
	(15, 2, 'Kraymorska 1'),
	(16, 1, 'Pozitano 3'),
	(17, 1, 'Zhitnitsa 25'),
	(18, 2, 'Tulcha 44'),
	(19, 2, 'Ostrava 7'),
	(21, 2, 'Damyanitsa 1'),
	(22, 2, 'Kom 22'),
	(23, 8, 'Planinets 5'),
	(24, 9, 'Belgielei 34'),
	(25, 8, 'Biser 26'),
	(27, 9, 'Zangvogelstraat 38'),
	(28, 1, 'Nezabravka 13'),
	(29, 5, 'Heusteigstrabe 96'),
	(30, 6, 'Tegzes 112'),
	(31, 6, 'Remete 125'),
	(41, 10, '17 Ave SW'),
	(42, 1, 'Pliska 1'),
	(43, 7, 'Ehitajate tee 39'),
	(44, 1, 'Klokotnitsa 10'),
	(46, 2, 'Gradus 5'),
	(48, 3, 'Kray Gorata 1'),
	(50, 1, 'Rila 61'),
	(52, 7, 'Laagna 187'),
	(53, 3, 'Predelna 6'),
	(63, 3, 'Sredna Gora 25'),
	(64, 13, 'Giannitson 85'),
	(65, 9, 'Gininanto 1'),
	(66, 7, 'Budice 12'),
	(67, 9, 'Kulita 25'),
	(69, 15, 'Rue Verbist 26'),
	(70, 5, 'Veem Strase 5'),
	(71, 1, 'Pliska 12'),
	(72, 1, 'Morska 6'),
	(73, 2, 'Kraymorsa 12');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Dumping data for table deliverit.categories: ~7 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `category`) VALUES
	(2, 'Clothing'),
	(11, 'Cosmetics'),
	(1, 'Electronics'),
	(5, 'Furniture'),
	(10, 'Home textiles'),
	(3, 'Medical'),
	(4, 'Toys');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table deliverit.cities: ~15 rows (approximately)
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` (`id`, `city`, `countries_id`) VALUES
	(1, 'Sofia', 1),
	(2, 'Varna', 1),
	(3, 'Plovdiv', 1),
	(4, 'Bucarest', 2),
	(5, 'Stutgart', 3),
	(6, 'Budapest', 5),
	(7, 'Tallinn', 6),
	(8, 'Gabrovo', 1),
	(9, 'Antwerp', 13),
	(10, 'Calgary', 14),
	(11, 'Rabat', 15),
	(12, 'Rijeka', 17),
	(13, 'Karditsa', 18),
	(14, 'Valencia', 19),
	(15, 'Brussels', 13);
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;

-- Dumping data for table deliverit.countries: ~13 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `country`) VALUES
	(13, 'Belgium'),
	(1, 'Bulgaria'),
	(14, 'Canada'),
	(17, 'Croatia'),
	(6, 'Estonia'),
	(4, 'France'),
	(3, 'Germany'),
	(18, 'Greece'),
	(5, 'Hungary'),
	(20, 'India'),
	(15, 'Morocco'),
	(2, 'Romania'),
	(19, 'Spain');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping data for table deliverit.parcels: ~26 rows (approximately)
/*!40000 ALTER TABLE `parcels` DISABLE KEYS */;
INSERT INTO `parcels` (`id`, `weight`, `warehouses_id`, `categories_id`, `shipments_id`, `users_id`, `delivered_to_address`, `is_deleted`) VALUES
	(16, 21.000, 1, 2, 1, 12, 0, 0),
	(17, 1.000, 3, 1, 2, 2, 1, 0),
	(18, 39.000, 3, 3, 3, 3, 0, 1),
	(19, 21.000, 1, 2, 1, 3, 0, 1),
	(20, 12.000, 3, 3, 3, 3, 0, 0),
	(21, 12.000, 3, 1, 1, 5, 1, 0),
	(22, 29.000, 3, 1, 1, 3, 1, 0),
	(23, 6.000, 3, 3, 3, 3, 0, 0),
	(24, 16.000, 3, 1, 1, 2, 0, 0),
	(25, 10.000, 1, 2, 3, 5, 1, 0),
	(26, 11.000, 1, 2, 3, 3, 0, 0),
	(27, 11.000, 1, 2, 3, 3, 0, 1),
	(28, 11.000, 1, 2, 1, 5, 0, 0),
	(29, 12.000, 1, 2, 3, 3, 0, 0),
	(30, 11.254, 1, 2, 3, 3, 0, 0),
	(31, 11.000, 1, 2, 3, 3, 0, 0),
	(32, 11.000, 2, 2, 3, 3, 0, 0),
	(33, 3.000, 1, 2, 9, 3, 0, 0),
	(36, 1.230, 7, 1, 1, 6, 0, 0),
	(37, 2.500, 31, 4, 4, 14, 0, 0),
	(38, 0.390, 1, 2, 8, 16, 0, 0),
	(39, 1.680, 27, 1, 9, 12, 0, 0),
	(41, 2.590, 27, 2, 9, 12, 1, 0),
	(42, 0.590, 31, 1, 7, 7, 1, 0),
	(43, 1.500, 2, 11, 1, 6, 1, 0),
	(44, 2.580, 3, 2, 11, 24, 0, 0);
/*!40000 ALTER TABLE `parcels` ENABLE KEYS */;

-- Dumping data for table deliverit.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role`) VALUES
	(1, 'customer'),
	(2, 'employee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table deliverit.shipments: ~11 rows (approximately)
/*!40000 ALTER TABLE `shipments` DISABLE KEYS */;
INSERT INTO `shipments` (`id`, `origin_wh_id`, `delivery_wh_id`, `departure_date`, `arrival_date`, `status_id`, `is_deleted`) VALUES
	(1, 1, 2, '2021-08-10', '2021-08-10', 1, 0),
	(2, 2, 3, '2021-08-11', '2021-09-09', 2, 0),
	(3, 1, 3, '2021-08-12', '2021-08-15', 2, 0),
	(4, 25, 31, '2021-08-26', '2021-08-26', 3, 0),
	(5, 25, 31, '2021-08-26', '2021-08-26', 2, 0),
	(6, 25, 30, '2021-08-26', '2021-08-26', 3, 0),
	(7, 28, 31, '2021-08-26', '2021-08-26', 3, 0),
	(8, 1, 1, '2021-08-10', '2021-08-26', 3, 0),
	(9, 28, 27, '2021-08-26', '2021-08-30', 2, 0),
	(10, 2, 3, '2021-08-12', '2021-08-13', 2, 0),
	(11, 28, 3, '2021-09-05', '2021-09-13', 1, 0);
/*!40000 ALTER TABLE `shipments` ENABLE KEYS */;

-- Dumping data for table deliverit.statuses: ~3 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`id`, `status`) VALUES
	(3, 'completed'),
	(2, 'on the way'),
	(1, 'preparing');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping data for table deliverit.users: ~17 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `addresses_id`, `username`, `password`, `roles_id`, `is_deleted`) VALUES
	(2, 'Milen', 'Petkov', 'petkov@abv.bg', 2, 'milen', '$2b$10$VKIfrqPPM/HAOPqagkYlMOn0dreleP072wM4c22EmUiceg01u69Ga', 2, 0),
	(3, 'Kristins', 'Dimitrova', 'kristins@abv.bg', 1, 'kristins', '$2b$10$ZV/8o3j9X5ZeAzCTi9Qwauj115t2MrN2FtZuFcpXcJ06KLyOjFeX2', 2, 0),
	(5, 'Marina', 'Gateva', 'marina@abv.bg', 3, 'marina', '$2b$10$AiZGe1kL0.HU9eq6TDslfOq2i/tMdIE7IjFO0Mno5BzfxlQzAx3SO', 1, 0),
	(6, 'Mickey', 'Manuchi', 'mickey@abv.bg', 4, 'mickey', '$2b$10$Q3MZ5onUP7T9YDrFqtBVMuSFe7kXskq9awQPG.gOnu6w/lvJ.C7ca', 2, 0),
	(7, 'Nikola', 'Matev', 'nezabravka@abv.bg', 27, 'nikola', '$2b$10$zXPjxeH98p71QZWz/4yV1uWbh65H1B4sqpurJZc1XnrDgt3.Ud21G', 2, 0),
	(8, 'Zabravka', 'Zagorska', 'zabravka@abv.bg', 28, 'zabravka', '$2b$10$TUdTyke3c0zwFUjB5Sq.p.cbsehPCm2nmnVte6LhmtCsOKQCGH4CG', 1, 0),
	(9, 'Zlatin', 'Zlatev', 'zlatin@abv.bg', 29, 'zlatin', '$2b$10$I7ImqDehVt5otNKjiE6IK.FtBb.0S6Bt7ixmV5qoKLNNbX7hsn15O', 1, 0),
	(10, 'Bilyana', 'Yordanova', 'bilyana@abv.bg', 42, 'bilyana', '$2b$10$ihrQUHM.1bFUP.YmjfFXa.xlawbgoL3ezrYMT5sw2oghS/GDIVt7G', 1, 0),
	(12, 'Alexander', 'Milkov', 'alexandro@abv.bg', 44, 'alexandro', '$2b$10$ThpSO7unLt3QSisC/hYEkeIDm0FW0SgNEA7/2Hvb6uRewM6LT.PH.', 1, 0),
	(14, 'Kalina', 'Arnaudova', 'kalinamalina@abv.bg', 46, 'kalinamalina', '$2b$10$bEv7XJ8hLBh8sdBIwWBk2eFmLN2xSWfhQFFCXH5Pbzy6UPlHsnXKG', 1, 0),
	(16, 'Dimitar', 'Petrov', 'dimitar@abv.bg', 48, 'dimitar', '$2b$10$ObH8co43VBuGXrTgBg0HAer6qDT9WJlh4.cVjCFMHNrKkRo1RVtdy', 1, 0),
	(18, 'Boris', 'Peshev', 'boris@abv.bg', 50, 'boris', '$2b$10$dbdIY0P5MxDR7foScIytNe0DiAbGGMIoN5138OLF1UeEgcYkkM8Bu', 1, 0),
	(19, 'Grigor', 'Dimchev', 'grigor@abv.bg', 52, 'grigor', '$2b$10$6nY5T.t/qjONY0cdJlB8aOUlK.OUkf0GpbSUYoqJXJpIvQ7Gr6vAO', 1, 0),
	(20, 'Konstantin', 'Stanchev', 'kotceto@abv.bg', 63, 'kotceto', '$2b$10$W45ODRXbMvtZ0bWhpOO8defZYH1ReraPEwaIQilZyloCGmwC8MO6y', 1, 0),
	(21, 'Ivan', 'Spasov', 'ivancho@abv.bg', 67, 'ivancho', '$2b$10$vIaJZO5.Lt5l1e/ve1ggZeraDlO7bGx6/x0f4FgbCAvSl9ZqzEJ1K', 1, 0),
	(22, 'Marina', 'Gateva', 'marina@abv.bg', 1, 'mari na', '$2b$10$kWfKxvVl2ZVx9cSSjJT8aehZShgOth3B4Jcmo.lqZ9ZQeIRU/6pry', 1, 0),
	(24, 'Borislav', 'Arnaudov', 'borko@abv.bg', 73, 'borko', '$2b$10$wruYMiVFZKlNqVbCHyzRyuY9rFwjqbAl6SMpBVflrEXm77koP/dTK', 1, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table deliverit.warehouses: ~16 rows (approximately)
/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
INSERT INTO `warehouses` (`id`, `warehouse`, `addresses_id`, `is_deleted`) VALUES
	(1, 'Bucarest18', 2, 0),
	(2, 'Stutgart5', 4, 0),
	(3, 'Plovdiv6', 12, 0),
	(7, 'Rijeka1', 3, 0),
	(25, 'Sofiya1', 16, 0),
	(26, 'Sofiya2', 42, 0),
	(27, 'Sofiya3', 44, 0),
	(28, 'Varna1', 14, 0),
	(29, 'Varna2', 15, 0),
	(30, 'Plovdiv1', 48, 0),
	(31, 'Bucarest1', 1, 0),
	(32, 'Karditsa2', 64, 0),
	(33, 'Antwerp16', 65, 0),
	(34, 'Tallinn10', 66, 0),
	(36, 'Brussels 3', 69, 0),
	(37, 'Sofia20', 71, 0);
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
