import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';

import authController from './controllers/auth-controller.js';
import usersController from './controllers/users-controller.js';
import countriesController from './controllers/countries-controller.js';
import citiesController from './controllers/cities-controller.js';
import addressController from './controllers/address-controller.js';
import categoriesController from './controllers/categories-controller.js';
import warehousesController from './controllers/warehouse-controller.js';
import shipmentsController from './controllers/shipments-controller.js';
import parcelController from './controllers/parcels-controller.js';

const app = express();

const config = dotenv.config().parsed;
const PORT = +config.PORT;

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use(express.json());
app.use(cors());
app.use(helmet());

app.use('/auth', authController);
app.use('/users', usersController);
app.use('/countries', countriesController);
app.use('/cities', citiesController);
app.use('/address', addressController);
app.use('/categories', categoriesController);
app.use('/warehouses', warehousesController);
app.use('/shipments', shipmentsController);
app.use('/parcels', parcelController);

app.all('*', (req, res) =>
    res.status(400).json({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`App is listening at port ${PORT}`));
