export const TOKEN_LIFETIME = 60 * 60 * 24 * 31;

export const DEFAULT_USER_ROLE = 'customer';

export const STATUS = {
    PREPARING : 1,
    ON_THE_WAY : 2,
    COMPLETED : 3
}