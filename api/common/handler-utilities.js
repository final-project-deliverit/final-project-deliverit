export const sendResponse = ({error, data}, req, res) => {
  if (error) {
    console.log(error.message);
    return res.status(error.httpCode).json({ message: error.message });
  }
  
  const successCode = req.method === 'POST' ? 201 : 200;

  return res.status(successCode).json(data);
};
