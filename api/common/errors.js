export default {
  
  NOT_FOUND: { 
    message: 'Resource not found', 
    httpCode: 404,
  }, 
  
  OPERATION_NOT_ALLOWED: {
    message: 'Operation not allowed',
    httpCode: 405,
  },
  
  OPERATION_FAILURE: { 
    message: 'Operation failure',
    httpCode: 409,
  },

  DUPLICATE_RECORD: { 
    message: 'Resource already exists',
    httpCode: 409,
  },

  BAD_REQUEST: {
    message: 'Bad request',
    httpCode: 400,
  },
  
};
