export default {
    id: (value) => typeof value === 'number',
    cities_id: (value) => typeof value === 'number',
    street_name: (value) => typeof value === 'string' && value.length > 4 && value.length < 45,
  };
  