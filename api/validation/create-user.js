import * as EmailValidator from 'email-validator';

export default {
    username: (value) => typeof value === 'string' && value.length > 5 && value.length < 15 && !(value.includes(' ')),
    password: (value) => (typeof value === 'string' || typeof value === 'number') && value.length > 5 && value.length < 45,
    addresses_id: (value) => typeof value === 'number',
    email: (value) => EmailValidator.validate(value),
    first_name: (value) => typeof value === 'string' && value.length > 4 && value.length < 45,
    last_name: (value) => typeof value === 'string' &&  value.length > 4 && value.length < 45,
  };
  