import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import addressData from '../data/address-data.js';
import addressService from '../services/address-service.js';


const addressController = express.Router();

addressController
    .get('/:id', async (req, res) => {
        const data = req.params.id;
        const address = await addressService.showAddressById(addressData)(data);

        res.status(200).json(address);
    })
    .post('/',  async (req, res) => {
        const data = req.body;
        const createdAddress = await addressService.addAddress(addressData)(data);

        res.status(201).json(createdAddress);
    })

    .put('/', authMiddleware, async (req, res) => {
        const data = req.body;
        const updatedAddress = await addressService.changeAddress(addressData)(data);

        res.status(200).json(updatedAddress);
    });

export default addressController;
