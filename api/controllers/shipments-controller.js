import express from 'express';
import shipmentsData from '../data/shipments-data.js';
import shipmentsServices from '../services/shipments-services.js';
import { authMiddleware } from '../auth/auth.middleware.js';

const shipmentsController = express.Router();

shipmentsController

    .get('/', authMiddleware, async (req, res) => {
        const shipments = await shipmentsServices.showShipments(shipmentsData)();

        res.status(200).json(shipments);
    })
    .get('/with/:id', authMiddleware, async (req, res) => {
        const id = req.params.id;
        const shipments = await shipmentsServices.showShipmentById(shipmentsData)(id);

        res.status(200).json(shipments);
    })
    .get('/status/:id', authMiddleware, async (req, res) => {
        const id = req.params.id;
        const shipments = await shipmentsServices.showShipmentsWithStatus(shipmentsData)(id);

        res.status(200).json(shipments);
    })
    .get('/by', authMiddleware, async (req, res) => {
        const { column } = req.query;

        const shipments = await shipmentsServices.filterShipmentsByWh(shipmentsData)(column);

        res.status(200).json(shipments);
    })
    .post('/', authMiddleware, async (req, res) => {
        const data = req.body;
        const createdShipment = await shipmentsServices.addShipment(shipmentsData)(data);

        res.status(201).json(createdShipment);
    })
    .put('/', async (req, res) => {
        const data = req.body;
        const updatedShipment = await shipmentsServices.changeShipment(shipmentsData)(data);

        res.status(200).json(updatedShipment);
    })
    .put('/d', async (req, res) => {
        const data = req.body;
        const deletedShipment = await shipmentsServices.removeShipment(shipmentsData)(data);

        res.status(200).json(deletedShipment);
    });

export default shipmentsController;