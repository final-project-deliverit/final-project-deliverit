import express from 'express';
import countriesService from '../services/countries-service.js';
import countriesData from '../data/countries-data.js';
import { authMiddleware } from '../auth/auth.middleware.js'; 

const countriesController = express.Router();

countriesController

    .get('/', async (req, res) => {
        const countries = await countriesService.showCountries(countriesData)();

        res.status(200).json(countries);
    });

export default countriesController;
