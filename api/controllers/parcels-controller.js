import express from 'express';
import parcelsService from '../services/parcels-service.js';
import parcelsData from '../data/parcels-data.js';
import { authMiddleware } from '../auth/auth.middleware.js';

const parcelController = express.Router();

parcelController

    .get('/', authMiddleware, async (req, res) => {
        const parcels = await parcelsService.showParcels(parcelsData)();

        res.status(200).json(parcels);
    })

    .get('/parcel/:id', authMiddleware, async (req, res) => {
        const value = req.params.id;
        const parcels = await parcelsService.showParcelById(parcelsData)(value);

        res.status(200).json(parcels);
    })

    .get('/category/:id', authMiddleware, async (req, res) => {
        const value = req.params.id;
        const parcels = await parcelsService.filterParcelsByCategory(parcelsData)(value);

        res.status(200).json(parcels);
    })

    .get(`/weight`, authMiddleware, async (req, res) => {
        const minValue = req.query.minWeight;
        const maxValue = req.query.maxWeight;

        const parcels = await parcelsService.filterParcelsByWeight(parcelsData)(minValue, maxValue);

        res.status(200).json(parcels);
    })

    .get('/warehouse/:id', authMiddleware, async (req, res) => {
        const value = req.params.id;
        const parcels = await parcelsService.filterParcelsByWarehouse(parcelsData)(value);

        res.status(200).json(parcels);
    })
    .get('/shipment/:id', authMiddleware, async (req, res) => {
        const id = req.params.id;
        const parcels = await parcelsService.filterParcelsByShipment(parcelsData)(id);

        res.status(200).json(parcels);
    })

    .get('/customer/:id', authMiddleware, async (req, res) => {
        const value = req.params.id;
        const parcels = await parcelsService.filterParcelsByCustomer(parcelsData)(value);

        res.status(200).json(parcels);
    })
    .get('/status/:id', authMiddleware, async (req, res) => {
        const value = req.params.id;
        const parcels = await parcelsService.filterParcelsByStatus(parcelsData)(value);

        res.status(200).json(parcels);
    })

    .post('/', authMiddleware, async (req, res) => {
        const data = req.body;
        const createdParcel = await parcelsService.addParcel(parcelsData)(data);

        res.status(201).json(createdParcel);
    })

    .put('/', authMiddleware, async (req, res) => {
        const data = req.body;
        const updatedParcel = await parcelsService.changeParcel(parcelsData)(data);

        res.status(200).json(updatedParcel);
    })

    .put('/d', authMiddleware, async (req, res) => {
        const data = req.body;
        const deletedParcel = await parcelsService.removeParcel(parcelsData)(data);

        res.status(200).json(deletedParcel);
    })
    .put('/shipment', authMiddleware, async (req, res) => {
        const { id, ship_id } = req.body;
        const deletedParcel = await parcelsService.updateShipId(parcelsData)(id, ship_id);

        res.status(200).json(deletedParcel);
    })
export default parcelController;    
