import express from 'express';
import citiesService from '../services/cities-service.js';
import citiesData from '../data/cities-data.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import errors from '../services/service-errors.js';

const citiesController = express.Router();

citiesController

    .get('/', async (req, res) => {
        const cities = await citiesService.showCities(citiesData)();

        res.status(200).json(cities);
    })

    .get('/:id', async (req, res) => {
        const { id } = req.params;

        const { error, cities } = await citiesService.getCitiesByCountryId(citiesData)(+id);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'Country not found!' });
        } else {
            res.status(200).send(cities);
        }
    });

export default citiesController;
