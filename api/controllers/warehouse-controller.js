import express from 'express';
import warehousesService from '../services/warehouses-service.js';
import warehousesData from '../data/warehouses-data.js';
import { authMiddleware } from '../auth/auth.middleware.js';

const warehousesController = express.Router();

warehousesController

    .get('/', async (req, res) => {
        const wh = await warehousesService.showWarehouses(warehousesData)();

        res.status(200).json(wh);
    })
    .get('/:id', authMiddleware, async (req, res) => {
        const id = req.params.id;
        const resWh = await warehousesService.getWhById(warehousesData)(id);

        res.status(200).json(resWh);
    })
    .get('/p', authMiddleware, async (req, res) => {
        const wh = await warehousesService.showWarehousesPublic(warehousesData)();

        res.status(200).json(wh);
    })

    .post('/', authMiddleware, async (req, res) => {
        const data = req.body;
        const createdWarehouse = await warehousesService.addWarehouse(warehousesData)(data);

        res.status(201).json(createdWarehouse);
    })

    .put('/', async (req, res) => {
        const data = req.body;
        const updatedWarehouse = await warehousesService.changeWarehouse(warehousesData)(data);

        res.status(200).json(updatedWarehouse);
    })
    .put('/d', async (req, res) => {
        const data = req.body;
        const deletedWarehouse = await warehousesService.removeWarehouse(warehousesData)(data);

        res.status(200).json(deletedWarehouse);
    });
export default warehousesController;