import express from 'express';
import { authMiddleware } from '../auth/auth.middleware.js';
import categoriesData from '../data/categories-data.js';
import categoriesService from '../services/categories-service.js';
import { sendResponse } from './../common/handler-utilities.js';

const categoriesController = express.Router();

categoriesController
    .get('/', authMiddleware, 
        async (req, res) => {
            const categories = await categoriesService.showCategories(categoriesData)();

            // res.status(200).json(categories);
            return sendResponse(categories, req, res);
        }
    )

export default categoriesController;  
