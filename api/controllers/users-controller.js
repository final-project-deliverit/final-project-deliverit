import express from 'express';
import usersData from '../data/users-data.js';
import userService from '../services/user-service.js';
import errors from '../services/service-errors.js';
import { authMiddleware } from '../auth/auth.middleware.js';


const usersController = express.Router();

usersController
    // get all customers with searching name
    .get('/', async (req, res) => {
        const { q } = req.query;
        const users = await userService.getCustomers(usersData)(q);
        res.status(200).send(users);
    })
    //search customers by email
    .get('/customers', async (req, res) => {
        const { q } = req.query;
        const customers = await userService.searchCustomersByEmail(usersData)(q);

        res.status(200).send(customers);
    })

    // get user by id    
    .get('/:id', authMiddleware, 
        async (req, res) => {
            const { id } = req.params;

            const { error, user } = await userService.getUserById(usersData)(+id);

            if (error === errors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else {
                res.status(200).send(user);
            }
        })
    // create user
    .post('/',  async (req, res,) => {
        const createData = req.body;

        const { error, user } = await userService.createUser(usersData)(createData);
        if (error === errors.DUPLICATE_RECORD) {
            res.status(409).send({ message: 'Name not available' });
        } else {
            res.status(201).send(user);
        }
    })

    // update user by id
    .put('/:id', 
        async (req, res) => {
            const { id } = req.params;
            const updateData = req.body;

            const userId = req.user.id;
            const role = req.user.role;

            const { error, user } = await userService.updateUser(usersData)(+id, updateData, role, userId);

            if (error === errors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else if (error === errors.DUPLICATE_RECORD) {
                res.status(409).send({ message: 'Name not available' });
            } else if (error === errors.UNALLOWED_ACTION) {
                res.status(405).json({ message: 'Unallowed action' });
            } else {
                res.status(200).send(user);
            }
        })
    // delete user by id
    .delete('/:id', authMiddleware, async (req, res) => {
        const { id } = req.params;
        const { error, user } = await userService.removeCustomer(usersData)(+id);

        if (error === errors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'User not found!' });
        } else {
            res.status(204).send(user);
        }
    })

export default usersController;
