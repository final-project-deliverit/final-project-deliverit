import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

const options = {
    secretOrKey: SECRET_KEY,
    jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
    const userData = {
        id: payload.id,
        username: payload.username,
        role: payload.role,
    };
    // userData will be set as `req.user` in the `next` middleware
    done(null, userData);
});

export default jwtStrategy;
