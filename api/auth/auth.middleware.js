import passport from 'passport';

const authMiddleware = (req, res, next) => {
    passport.authenticate('jwt', { session: false })(req, res, next);
};

const roleMiddleware = roleName => {
    return (req, res, next) => {
        if (req.user && req.user.role === roleName) {
            next();
        } else {
            res.status(403).send({
                message: 'Resource is forbidden.'
            });
        }
    };
};

export {
    authMiddleware,
    roleMiddleware
};
