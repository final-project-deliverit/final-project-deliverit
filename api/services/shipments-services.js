
const showShipments = shipmentsData => async (data) => {

    return await shipmentsData.getShipments(data);
};
const showShipmentById = shipmentsData => async (id) => {

    return await shipmentsData.getShipmentById(id);
};
const showShipmentsWithStatus = shipmentsData => async (id) => {

    return await shipmentsData.getShipmentsWithStatus(id);
};
const addShipment = shipmentsData => async (data) => {

    return await shipmentsData.createShipment(data);
};
const changeShipment = shipmentsData => async (data) => {

    return await shipmentsData.updateShipment(data);
};
const removeShipment = shipmentsData => async (data) => {

    return await shipmentsData.deleteShipment(data);
};
const filterShipmentsByWh = shipmentsData => async (column) => {

    return await shipmentsData.filterByWarehouse(column);
};
export default {
    showShipments,
    showShipmentById,
    showShipmentsWithStatus,
    addShipment,
    changeShipment,
    removeShipment,
    filterShipmentsByWh,
};