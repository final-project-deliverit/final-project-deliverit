
const showCategories = categoriesData => async () => {

    return await categoriesData.getCategories();
};

export default {
    showCategories,
};
