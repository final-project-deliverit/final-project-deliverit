import errors from './service-errors.js';

const showCities = citiesData => async () => {

    return await citiesData.getAllCities();
};

const getCitiesByCountryId = citiesData => {
    return async (country_id) => {
        const cities = await citiesData.getCitiesInCountry(country_id);

        if (cities.length === 0) {
            return {
                error: errors.RECORD_NOT_FOUND,
                cities: null
            };
        }

        return { error: null, cities: cities };
    };
};

export default {
    showCities,
    getCitiesByCountryId,
};