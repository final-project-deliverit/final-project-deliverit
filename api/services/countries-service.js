
const showCountries = countriesData => async () => {

    return await countriesData.getAllCountries();
};

export default {
    showCountries,
};
