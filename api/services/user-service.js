import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE } from '../common/constants.js';
import errors from './service-errors.js';

const signInUser = usersData => {
    return async (username, password) => {
        const user = await usersData.getWithRole(username);
        if (!user || !(await bcrypt.compare(password, user.password))) {
            return {
                error: errors.INVALID_SIGNIN,
                user: null
            };
        }

        return {
            error: null,
            user: user
        };
    };
};

const getUserById = usersData => {
    return async (id) => {
        const user = await usersData.getBy(id);

        if (!user) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }

        return { error: null, user: user };
    };
};

const getCustomers = usersData => {
    return async (filter) => {
        return filter ?
        await usersData.searchByName(['first_name', 'last_name'], filter)
        : await usersData.getAllCustomers();
    };
};

const searchCustomersByEmail = usersData => {
    return async (q) => {
        return await usersData.searchByEmail('email', q)
    }
}

const createUser = usersData => {
    return async (userCreate) => {
        const { username, password, addresses_id, first_name, last_name, email } = userCreate;

        const existingUser = await usersData.checkForDuplicateUsername('username', username);
        if (existingUser) {
            return {
                error: errors.DUPLICATE_RECORD,
                user: null
            };
        }

        const passwordHash = await bcrypt.hash(password, 10);
        const user = await usersData.create(username, passwordHash, DEFAULT_USER_ROLE, addresses_id, first_name, last_name, email);

        return { error: null, user: user };
    };
};

const updateUser = usersData => {
    return async (id, userUpdate, role, userId) => {
        const user = await usersData.getBy(id);
        if (!user) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }
        if (role === DEFAULT_USER_ROLE) {
            if (id !== userId) {
                return {
                    error: errors.UNALLOWED_ACTION,
                    user: null,
                };
            }
        }
        if (userUpdate.username && !(await usersData.getBy('username', userUpdate.username))) {
            return {
                error: errors.DUPLICATE_RECORD,
                user: null
            };
        }

        const updated = { ...user, ...userUpdate };
        const _ = await usersData.update(updated);

        return { error: null, user: updated };
    };
};

const removeCustomer = usersData => {
    return async (id) => {
        const userToDelete = await usersData.getBy(id);
        if (!userToDelete) {
            return {
                error: errors.RECORD_NOT_FOUND,
                user: null
            };
        }

        const _ = await usersData.deleteCustomer(userToDelete);

        return { error: null, user: userToDelete };
    };
};

export default {
    signInUser,
    getUserById,
    getCustomers,
    searchCustomersByEmail,
    createUser,
    updateUser,
    removeCustomer,
};
