
const showParcels = parcelsData => async () => {

    return await parcelsData.getParcels();
};

const showParcelById = parcelsData => async (data) => {

    return await parcelsData.getParcelById(data);
};

const addParcel = parcelsData => async (data) => {

    return await parcelsData.createParcel(data);
};

const changeParcel = parcelsData => async (data) => {

    return await parcelsData.updateParcel(data);
};

const removeParcel = parcelsData => async (data) => {

    return await parcelsData.deleteParcel(data);
};

const filterParcelsByWeight = parcelsData => async (minValue, maxValue) => {

    return await parcelsData.filterByWeight(minValue, maxValue);
};

const filterParcelsByCustomer = parcelsData => async (value) => {

    return await parcelsData.filterByCustomer(value);
};

const filterParcelsByWarehouse = parcelsData => async (value) => {

    return await parcelsData.filterByWarehouse(value);
};
const filterParcelsByShipment = parcelsData => async (value) => {

    return await parcelsData.filterByShipment(value);
};

const filterParcelsByCategory = parcelsData => async (value) => {

    return await parcelsData.filterByCategory(value);
};
const filterParcelsByStatus = parcelsData => async (value) => {

    return await parcelsData.filterByStatus(value);
};
const updateShipId = shipmentsData => async (column, value) => {

    return await shipmentsData.updateShipmentId(column, value);
};


export default {
    showParcels,
    showParcelById,
    addParcel,
    changeParcel,
    removeParcel,
    filterParcelsByWeight,
    filterParcelsByCustomer,
    filterParcelsByWarehouse,
    filterParcelsByShipment,
    filterParcelsByCategory,
    filterParcelsByStatus,
    updateShipId
};