
const showWarehouses = warehousesData => async () => {

    return await warehousesData.getWarehouses();
};
const showWarehousesPublic = warehousesData => async () => {

    return await warehousesData.getWarehouses();
};

const addWarehouse = warehousesData => async (whData) => {

    return await warehousesData.createWarehouse(whData);
};

const changeWarehouse = warehousesData => async (data) => {

    return await warehousesData.updateWarehouse(data);
};

const removeWarehouse = warehousesData => async (data) => {

    return await warehousesData.deleteWarehouse(data);
};
const getWhById = warehousesData => async (id) => {

    return await warehousesData.getById(id);
};

export default {
    showWarehouses,
    showWarehousesPublic,
    addWarehouse,
    changeWarehouse,
    removeWarehouse,
    getWhById
};