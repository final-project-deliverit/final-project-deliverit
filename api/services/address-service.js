const addAddress = addressData => async (data) => {

    return await addressData.createAddress(data);
};

const changeAddress = addressData => async (data) => {

    return await addressData.updateAddress(data);
};

const showAddressById = addressData => async (id) => {

    return await addressData.getAddressById(id);
};

export default {
    addAddress,
    changeAddress,
    showAddressById,
};
