# Final project DeliverIT


## 1. Description

This is a web application that serves the needs of a freight forwarding company.

DeliverIT's customers can place orders on international shopping sites (like Amazon.de or eBay.com) and have their parcels delivered either to the company's warehouses or their own address.

DeliverIT has two types of users - customers and employees. The customers can see how many parcels they have on the way. The employees have a bit more capabilities. They can add new parcels to the system, modify existing ones, and check how many parcels has a given warehouse received and more.

## 2. Project information
  - Language and version: JavaScript ES2020
  - Platform and version: Node 14.0+
  - Core Packages: Express.js, React.js

## 3. Project architecture
  - database part  - MariaDB server
  - server part - build on Express.js package
  - client part - build on React.js package

## 4. Setup
####    4.1 Database
1. Create the database in a database tool, such as MySQL Workbench, from a file
```txt
    final-project-deliverit/api/deliverit_database.sql
```

2. Import the data into the database from a file
```txt
    final-project-deliverit/api/deliverit_database_data.sql
```

3. You can see the database chart in a file
   
```txt
	final-project-deliverit/stuff/DB
```
####    4.2 Back-End
1. Open with VSCode folder `final-project-deliverit/api`
2. Create `.env` file with the following content:
```js
    PORT=3001
    HOST=localhost
    DBPORT=3306
    USER=root
    PASSWORD=
    DATABASE=deliverit
    SECRET_KEY=kljduisfy758lskljf33
```

1. Add your database PASSWORD
2. Run `npm install` to restore all dependencies
3. Run `npm start` to start the server
4. Can find Postman Collection in 
   
```txt
    final-project-deliverit/stuff/DeliverIT.postman_collection.json
```
####    4.2 Front-End
1. Open with VSCode folder `final-project-deliverit/client`
2. Run `npm install` to restore all dependencies
3. Run `npm start` to start the application


## 5. Users
  - Registration form create only customers.
  - For explore the full functionality can use:

```Customer``` 
```txt
    Username: marina
    Password: 123456
```  
```Employee```  
```txt
    Username: mickey
    Password: 123456
```
